import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, ToastController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
/*
  Generated class for the UtilityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilityProvider {

  constructor(public http: HttpClient,
    private toastCtrl: ToastController,
    public atrCtrl: AlertController,
    private iab: InAppBrowser
  ) {
    console.log('Hello UtilityProvider Provider');
  }

  openYoutubeVideoUrl(vid){
    var link = "https://www.youtube.com/watch?v="+vid;
    const browser = this.iab.create(link,'_blank','location=no');
    browser.show();

        // window.open(link,'_blank', 'location=yes');
  }

  showAlert(msg) {
    const alert = this.atrCtrl.create({
      title: 'Alert !',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }



}
