import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

// Config
import {Config} from "../../config/main.config";

/*
  Generated class for the UserIngredientProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserIngredientProvider {

  endpoint: string = Config.api + '/user/ingredient';

  constructor(public http: HttpClient) {
    console.log('Hello UserIngredientProvider Provider');
  }

  add(token: string, body: any = {}) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.post(this.endpoint, body, {headers});
  }

}
