import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Storage} from "@ionic/storage";

// Config
import {Config} from '../../config/main.config';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  endpoint: string = Config.api + '/user';

  constructor(public http: HttpClient, private storage: Storage) {
    console.log('Hello UserProvider Provider');
  }

  isSubscribed() {
    return new Promise(async (resolve) => {
      await this.storage.ready();
      const user = await this.storage.get('menu_planner_user_data');
      resolve(user.is_subscribed);
    });
  }

  register(data) {
    return this.http.post(this.endpoint + '/auth/register', data);
  }

  getUser(token) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint, {
      headers
    });
  }

  login(data) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json"
    });
    return this.http.post(Config.api + '/login', data, {
      headers
    });
  }

  loginWithFacebook(params) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json"
    });
    return this.http.get(Config.api + '/login/facebook/callback', {
      headers,
      params
    });
  }

  loginWithGooglePlus(params) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json"
    });
    return this.http.get(Config.api + '/login/google/callback', {
      headers,
      params
    });
  }

  update(data, token) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.put(this.endpoint, data, {
      headers
    });
  }

  updateProviderDirect(token: string, body: any = {}) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.put(this.endpoint + '/provider', body, {
      headers
    });
  }

}
