import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

// Config
import {Config} from "../../config/main.config";

/*
  Generated class for the IngredientTypeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class IngredientTypeProvider {

  endpoint: string = Config.api + '/ingredient_type';

  constructor(public http: HttpClient) {
    console.log('Hello IngredientTypeProvider Provider');
  }

  getAll(token) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint, {
      headers
    });
  }

}
