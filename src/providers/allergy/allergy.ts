import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

// Config
import {Config} from '../../config/main.config';

/*
  Generated class for the AllergyProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AllergyProvider {

  endpoint: string = Config.api + '/allergies';

  constructor(public http: HttpClient) {
    console.log('Hello AllergyProvider Provider');
  }

  getAll(token) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint, {
      headers
    });
  }

  getAllByDiets(token, dietID) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint + '/by_diet/' + dietID, {
      headers
    });
  }

  limited() {
    return this.http.get(this.endpoint + '/limited');
  }

}
