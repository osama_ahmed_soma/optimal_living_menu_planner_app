import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

//Config
import {Config} from '../../config/main.config';
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";

/*
  Generated class for the IngredientProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class IngredientProvider {

  endpoint: string = Config.api + '/ingredients';

  private realIngredientData: any;
  private ingredients: any;

  constructor(public http: HttpClient, private storage: Storage, private events: Events,) {
    console.log('Hello IngredientProvider Provider');
  }

  manageIngredientsData({realData, isCatCompleted, isShowCompleteItem}) {
    return new Promise(resolve => {
      this.realIngredientData = realData;
      let totalIngredients = 0;
      this.storage.get('completedIngredients').then(completedIngredients => {
        if (completedIngredients) {
          for (const title in this.realIngredientData) {
            for (const index in this.realIngredientData[title]) {
              if (completedIngredients.length > 0) {
                for (const completedIngredient of completedIngredients) {
                  if (completedIngredient.id === this.realIngredientData[title][index].id) {
                    this.realIngredientData[title][index] = completedIngredient;
                  }
                }
              }
            }
            realData[title] = {
              data: this.realIngredientData[title],
              isCompleted: false
            };
          }
          this.ingredients = realData;
          for (const title in this.realIngredientData) {
            if (isCatCompleted) {
              this.ingredients[title].isCompleted = isCatCompleted(this.ingredients, title);
            }
          }
          if (isShowCompleteItem) {
            isShowCompleteItem(this.ingredients);
          }
        } else {
          for (const title in this.realIngredientData) {
            realData[title] = {
              data: this.realIngredientData[title],
              isCompleted: false
            };
          }
          this.ingredients = realData;
        }
        for (const title in realData) {
          for (const ingredient of realData[title].data) {
            if (!ingredient.hasOwnProperty('isChecked') || (ingredient.hasOwnProperty('isChecked') && !ingredient.isChecked)) {
              totalIngredients++;
            }
          }
        }
        this.events.publish('Ingredient:total', totalIngredients);
        resolve(this.ingredients);
      });
    });
  }

  getDislike() {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json"
    });
    return this.http.get(this.endpoint + '/dislike', {
      headers
    });
  }

  getForAll(token) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint + '/for_all', {
      headers
    });
  }

  add(token, body) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.post(this.endpoint, body, {
      headers
    });
  }

}
