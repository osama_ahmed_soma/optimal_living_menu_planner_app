import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

// Config
import {Config} from '../../config/main.config';

/*
  Generated class for the ProPlanProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProPlanProvider {

  endpoint: string = Config.api + '/pro_plan';

  constructor(public http: HttpClient) {
    console.log('Hello ProPlanProvider Provider');
  }

  getAll(token) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint, {
      headers
    });
  }

}
