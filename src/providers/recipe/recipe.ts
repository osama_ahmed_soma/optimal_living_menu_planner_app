import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

// Config
import {Config} from '../../config/main.config';

/*
  Generated class for the RecipeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RecipeProvider {

  endpoint: string = Config.api + '/recipe';
  endpoint2: string = Config.api + '/recipe_group';
  endpoint3: string = Config.api + '/user/recipe';

  constructor(public http: HttpClient) {
    console.log('Hello RecipeProvider Provider');
  }

  getAllRecipeGroups(token){
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint2, {
      headers
    });
  }

  getAllRecipeChild(id, token){
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint2+"/"+id, {
      headers
    });
  }

  getAll(token) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint, {
      headers
    });
  }

  get({recipe_id, token}) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint + '/' + recipe_id, {
      headers
    })
  }

  getNutritionInfoByApi({data}) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      // "Authorization": "Bearer " + token
    });

    var edamam_endpoint = "https://api.edamam.com/api/nutrition-details?app_id=f8ed35da&app_key=2bce0e96526c311f31d0a4d29ee050cf";
    return this.http.post(edamam_endpoint,data, {
      headers
    })
  }

  postServingQuantity({serving, recipe_id, token}) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.put(this.endpoint3 + '/add_serving/' + recipe_id, {serving}, {
      headers
    });
  }

  getServingQuantity({recipe_id, token}) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint3 + '/get_serving/' + recipe_id, {
      headers
    });
  }

  getByFilters({token, body}) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.post(`${this.endpoint}/search`, body, {
      headers
    });
  }

}
