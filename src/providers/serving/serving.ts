import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

// Config
import {Config} from '../../config/main.config';

/*
  Generated class for the ServingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServingProvider {

  endpoint: string = Config.api + '/servings';

  constructor(public http: HttpClient) {
    console.log('Hello ServingProvider Provider');
  }

  getAll() {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json"
    });
    return this.http.get(this.endpoint, {
      headers
    });
  }

  getAllWithAuth(token) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint + '/with_auth', {
      headers
    });
  }

}
