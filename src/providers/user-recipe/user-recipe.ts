import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

// Config
import {Config} from '../../config/main.config';

/*
  Generated class for the UserRecipeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserRecipeProvider {

  endpoint: string = Config.api + '/user/recipe';
  endpoint2: string = Config.api + '/user/recipe_group_children';

  constructor(public http: HttpClient) {
    console.log('Hello UserRecipeProvider Provider');
  }

  getAll(token) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint, {
      headers
    });
  }

  getAllByPlan(token, body) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.post(this.endpoint2, body, {
      headers
    });
  }

  getPreviousMealPlan(token) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.get(this.endpoint + '/previous', {
      headers
    });
  }

  add(token, body) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.post(this.endpoint, body, {
      headers
    });
  }

  addNotes(token, {recipeID, notes}) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
    return this.http.put(this.endpoint + '/add_notes/' + recipeID, {notes}, {
      headers
    });
  }

}
