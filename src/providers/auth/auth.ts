import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';

// Config
import {Config} from "../../config/main.config";

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  endpoint: string = Config.api;

  accessToken: string = '';
  refreshToken: string = '';
  expiresIn: number = 0;
  added: number = 0;

  providerName: string = '';
  providerData: any = null;

  constructor(public http: HttpClient, private storage: Storage) {
    console.log('Hello AuthProvider Provider');
  }

  setTokens(data, isStored: boolean = true) {
    this.accessToken = data.access_token;
    this.refreshToken = data.refresh_token;
    this.expiresIn = data.expires_in;
    this.added = data.hasOwnProperty('added') ? data.added : Date.now();

    this.providerName = data.providerName;
    this.providerData = data.providerData;

    if (isStored) {
      this.storage.set('accessToken', this.accessToken);
      this.storage.set('refreshToken', this.refreshToken);
      this.storage.set('expiresIn', this.expiresIn);
      this.storage.set('added', this.added);

      this.storage.set('providerName', this.providerName);
      this.storage.set('providerData', this.providerData);
    }
  }

  removeData() {
    this.accessToken = '';
    this.refreshToken = '';
    this.expiresIn = 0;
    this.added = 0;

    this.providerName = '';
    this.providerData = null;

    this.storage.remove('accessToken');
    this.storage.remove('refreshToken');
    this.storage.remove('expiresIn');
    this.storage.remove('added');

    this.storage.remove('providerName');
    this.storage.remove('providerData');
  }

  getData(name: string = '*'): Promise<any> {
    return new Promise(async resolve => {
      if (name === '*') {
        resolve({
          accessToken: await this.storage.get('accessToken'),
          refreshToken: await this.storage.get('refreshToken'),
          expiresIn: await this.storage.get('expiresIn'),
          added: await this.storage.get('added'),

          providerName: await this.storage.get('providerName'),
          providerData: await this.storage.get('providerData'),
        });
      } else {
        resolve(await this.storage.get(name));
      }
    });
  }

  private updateToken() {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": `Bearer ${this.accessToken}`
    });
    alert(JSON.stringify(this.refreshToken));
    return this.http.post(this.endpoint + '/refresh_token', {
      refresh_token: this.refreshToken
    }, {
      headers
    });
  }

  refresh() {
    return new Promise(resolve => {
      const currentDateTimeInSeconds = parseInt(((Date.now() % 60000) / 1000).toFixed(0));
      const added = parseInt(((this.added % 60000) / 1000).toFixed(0));
      if ((currentDateTimeInSeconds + 28800) >= (added + this.expiresIn)) {
        // refresh token
        this.updateToken().subscribe(res => {
          // this.setTokens(res);
          this.setTokens({
            ...res,
            providerName: this.providerName,
            provider: this.providerData
          });
          resolve(true);
        });
      } else {
        // all good
        resolve(false);
      }
    });
  }

}
