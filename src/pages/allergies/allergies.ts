import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, AlertController, ModalController} from 'ionic-angular';
import {Storage} from '@ionic/storage';

// Helpers
import {HelperMethods} from "../../classes/HelperMethods";

// Providers
import {AllergyProvider} from '../../providers/allergy/allergy';

import { DislikeSearchPage } from '../dislike-search/dislike-search';

/**
 * Generated class for the AllergiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-allergies',
  templateUrl: 'allergies.html',
})
export class AllergiesPage {

  allergies: Array<any> = [];
  moreAllergies: Array<any> = [];
  selectedAllergies: Array<any> = [];

  isLoading: boolean = true;

  afterLogin: boolean = false;
  totalSteps: number = 5;

  isShowMore: boolean = true;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private toastCtrl: ToastController,
              private storage: Storage,
              private allergyService: AllergyProvider,
              public modalCtrl: ModalController,
              private alertCtrl: AlertController) {
    this.afterLogin = this.navParams.get('afterLogin') || false;
    if (this.afterLogin) {
      this.totalSteps = 4;
    }
    this.allergyService.limited().subscribe((res: any) => {
      // this.allergies = res.data;
      let index = 0;
      for (let allergy of res.data) {
        if (index < 6) {
          this.allergies.push(allergy);
        } else {
          this.moreAllergies.push(allergy);
        }
        index++;
      }
      this.isLoading = false;
      this.manageStoredData();
    });
  }

  private manageStoredData() {
    this.storage.get('selectedAllergies').then((val) => {
      if (Array.isArray(val) && val.length > 0) {
        for (let allergy of val) {
          if (allergy.isSelected) {
            this.activeAllergy(allergy);
          }
        }
      }
      this.isLoading = false;
    });
  }

  openAll() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Add Allergies');
    for (let allergy of this.moreAllergies) {
      alert.addInput({
        type: 'checkbox',
        label: allergy.name,
        value: allergy,
        checked: false
      });
    }
    alert.addButton({
      text: 'Okay',
      handler: selectedAllergy => {
        if (selectedAllergy.length <= 0) return;
        for (const allergy of selectedAllergy) {
          const index = HelperMethods.getIndexOfArray({
            nameOfField: 'id',
            value: allergy.id,
            array: this.moreAllergies
          });
          allergy.isSelected = true;
          this.allergies.push(allergy);
          this.moreAllergies.splice(index, 1);
        }
      }
    });
    alert.present();
  }

  private presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });

    toast.present();
  }

  private activeAllergy(a) {
    for (let allergy of this.allergies) {
      if (allergy.id === a.id) {
        allergy.isSelected = true;
        break;
      } else {
        // this.activeMoreAllergy(a);
      }
    }
  }

  private activeMoreAllergy(a) {
    let i = 0;
    for (let allergy of this.moreAllergies) {
      if (allergy.id === a.id) {
        allergy.isSelected = true;
        this.allergies.push(allergy);
        this.moreAllergies.splice(i, 1);
        break;
      }
      i++;
    }
    if (this.moreAllergies.length <= 0) {
      this.isShowMore = false;
    }
  }

  back() {
    this.navCtrl.pop();
  }

  async next() {
    try {
      // await this.checkAllergiesIsSelected();
      this.storage.set('selectedAllergies', this.allergies);
      this.navCtrl.push('DislikePage', {
        afterLogin: this.afterLogin
      });
    } catch (e) {
      return this.presentToast(e.message);
    }
  }

  openIngredientSearch(){
    let data = {dislikes: this.moreAllergies, heading: "Allergies"};
    let myModal = this.modalCtrl.create(DislikeSearchPage, data);
    myModal.onDidDismiss(data => {
      console.log(data);

      if(data){
        if(data.length > 0){
          for (const dislike of data) {
            const index = HelperMethods.getIndexOfArray({
              nameOfField: 'id',
              value: dislike.id,
              array: this.moreAllergies
            });
            dislike.isSelected = true;
            this.allergies.push(dislike);
            this.moreAllergies.splice(index, 1);
          }
  
        }
      }
      
    });
    myModal.present();
  }

  // checkAllergiesIsSelected() {
  //   return new Promise((resolve, reject) => {
  //     if (this.allergies.length > 0) {
  //       let isSelected: boolean = false;
  //       for (let allergy of this.allergies) {
  //         if (isSelected) {
  //           resolve();
  //           break;
  //         }
  //         if (allergy.isSelected) {
  //           isSelected = true;
  //         }
  //       }
  //       if (!isSelected) {
  //         reject({
  //           message: 'You have to select at least one allergy.'
  //         });
  //       }
  //     } else {
  //       reject({
  //         message: 'Allergies data not found.'
  //       });
  //     }
  //   });
  // }

}
