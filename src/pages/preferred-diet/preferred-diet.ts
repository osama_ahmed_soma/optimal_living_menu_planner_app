import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController} from 'ionic-angular';
import {Storage} from '@ionic/storage';

// Provider
import {DietProvider} from '../../providers/diet/diet';
import {UtilityProvider} from '../../providers/utility/utility';

// Helper
import {HelperMethods} from "../../classes/HelperMethods";

// Pages
import { YvideoPage } from "../yvideo/yvideo"

/**
 * Generated class for the PreferredDietPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-preferred-diet',
  templateUrl: 'preferred-diet.html',
})
export class PreferredDietPage {

  diets: Array<any> = [];
  selectedDiet: number = 0;

  afterLogin: boolean = false;
  totalSteps: number = 5;
  visibility = false;

  getDefaultImage: any = HelperMethods.getDefaultImage;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private storage: Storage,
              public utilityProvider: UtilityProvider,
              public modalCtrl: ModalController,
              private dietService: DietProvider) {
    this.afterLogin = this.navParams.get('afterLogin') || false;
    if (this.afterLogin) {
      this.totalSteps = 4;
    }
    this.dietService.preferred().subscribe(res => {
      const response: any = res;
      this.diets = response.data;

      

      setTimeout(()=>{
        this.visibility = true;
      }, 1000);

      setTimeout(()=>{
        this.visibility = false;
      }, 3000);






    });
    this.storage.get('selectedDiet').then((val) => {
      if (val) {
        if (this.diets.length > 0) {
          for (let diet of this.diets) {
            if (diet.id === val) {
              this.selectedDiet = this.diets.findIndex(x => x.id === val);
              break;
            }
          }
        }
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreferredDietPage');
  }

  next() {
    this.storage.set('selectedDiet', this.diets[this.selectedDiet].id);
    this.navCtrl.push('AllergiesPage', {
      afterLogin: this.afterLogin
    });
  }

  back() {
    this.navCtrl.setRoot('TutorialPage', {}, {
      animate: true,
      direction: 'back'
    });
  }

  watchVideo(diet){
    console.log(diet);
    this.utilityProvider.openYoutubeVideoUrl(diet.youtubeid);
    // let data = {diet: diet}
    // let myModal = this.modalCtrl.create(YvideoPage, {youtubeid: diet.youtubeid});
    // myModal.onDidDismiss(data => {
    //   console.log(data);
    // });
    // myModal.present();
  }

}
