import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreferredDietPage } from './preferred-diet';

@NgModule({
  declarations: [
    PreferredDietPage,
  ],
  imports: [
    IonicPageModule.forChild(PreferredDietPage),
  ],
})
export class PreferredDietPageModule {}
