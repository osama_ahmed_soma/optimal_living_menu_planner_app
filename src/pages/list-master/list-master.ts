import {Component} from '@angular/core';
import {
  IonicPage,
  ModalController,
  NavController,
  ViewController,
  ToastController,
  LoadingController,
  AlertController,
  Events
} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {UserProvider} from '../../providers/user/user';
import {UserRecipeProvider} from '../../providers/user-recipe/user-recipe';
import {RecipeProvider} from '../../providers/recipe/recipe';

// Helper
import {HelperMethods} from "../../classes/HelperMethods";
import {UpgradePage} from "../upgrade/upgrade";

@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {

  accessToken: string = '';
  isLoading: boolean = true;
  recipes: Array<any> = [];
  recipes_o: Array<any> = [];
  recipe_group: Array<any> = [];
  recipe_group_o: Array<any> = [];
  quickplans : Array<any> = [];
  searchedData: any = null;
  selectedRecipes: Array<any> = [];
  selectedPlanRecipes: Array<any> = [];
  is_subscribed: boolean = false;
  topTab = 'recepi';
  searchTerm1: string = '';
  searchTerm2: string = '';

  constructor(private navCtrl: NavController,
              private modalCtrl: ModalController,
              private viewCtrl: ViewController,
              private authService: AuthProvider,
              private userService: UserProvider,
              private userRecipeService: UserRecipeProvider,
              private recipeService: RecipeProvider,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private events: Events) {
    this.userService.isSubscribed().then((is_subscribed: boolean) => (this.is_subscribed = is_subscribed));
    this.authService.getData('accessToken').then(accessToken => {
      this.accessToken = accessToken;
      this.recipeService.getAll(this.accessToken).subscribe((recipes: any) => {
        this.recipes_o = recipes.data;
        this.recipes = recipes.data;

        this.recipes.forEach((rec) =>{
          rec.image = (rec.image.match(/\.(jpeg|jpg|gif|png)$/) != null) ? rec.image : 'assets/img/default_recipe.jpg';
        });

        console.log(this.recipes)



        this.stopLoading();
      }, err => {
        this.stopLoading();
      });
    });
    this.events.subscribe('Recipe:Add', this.selectRecipe.bind(this));
    this.events.subscribe('Recipe:Remove', this.unSelectRecipe.bind(this));
    this.events.subscribe('Recipe:OpenFromQs', this.openSelectedQsRecipe.bind(this));
  }

  ionViewWillUnload() {
    this.events.unsubscribe('Recipe:Add', null);
    this.events.unsubscribe('Recipe:Remove', null);
    this.events.unsubscribe('Recipe:OpenFromQs', null);
  }

  private startLoading(): void {
    this.isLoading = true;
  }

  private stopLoading(): void {
    this.isLoading = false;
  }

  private manageSearchedData() {
    return new Promise((resolve, reject) => {
      const checkCalories: boolean = this.searchedData.calories > 0;
      const checkedCourses: boolean = this.searchedData.courses.length > 0;
      const checkedDishTypes: boolean = this.searchedData.dishTypes.length > 0;
      const selectedSort: boolean = this.searchedData.sort.length > 0;
      if (!checkCalories && !checkedDishTypes && !selectedSort && !checkedCourses) {
        return reject();
      }
      resolve();
    });
  }

  searchModel() {
    let modal = this.modalCtrl.create('SearchPage', {searchedData: this.searchedData } );
    modal.onDidDismiss(async data => {


      this.searchedData = data;
      if (this.searchedData['isUpdated']) {
        this.startLoading();
        try {


          console.log(this.searchedData);
          await this.manageSearchedData();
          this.recipeService.getByFilters({
            token: this.accessToken,
            body: this.searchedData
          }).subscribe((res: any) => {

            console.log(res);

            if (res.data.length > 0) {
              this.recipes = res.data;
            } else {
              HelperMethods.presentToast('No Recipe Found.', 'error', {
                toastCtrl: this.toastCtrl
              });
            }
            this.stopLoading();
          });

        } catch (e) {
          this.stopLoading();
        }
      }
    });
    modal.present();
  }

  openRecipe(recipe) {
    this.navCtrl.push('ItemDetailPage', {
      recipe
    });
  }

  removeSelectedItem(recipe){
    // recipe.isChecked = false;
    var self = this;
    let index = this.recipes.indexOf(recipe);
    this.recipes[index]["isChecked"] = false;

    setTimeout(function(){
      console.log(self.selectedRecipes.length);
    }, 1000)


  }

  presentConfirm() {
    let alert = this.alertCtrl.create(HelperMethods.generateSubscriptionAlertOptions(() => {
      let modal = this.modalCtrl.create('UpgradePage');
      modal.onDidDismiss(async data => {

      });
      modal.present();
    }, null));
    alert.present();
  }

  private selectRecipe(r) {
    const index = HelperMethods.getIndexOfArray({nameOfField: 'id', value: r.id, array: this.recipes});
    this.triggerRecipe(index, true);
  }

  private openSelectedQsRecipe(r) {
    const index = HelperMethods.getIndexOfArray({nameOfField: 'id', value: r.id, array: this.recipes});
    this.openRecipe(this.recipes[index]);
  }

  private unSelectRecipe(r) {
    const index = HelperMethods.getIndexOfArray({nameOfField: 'id', value: r.id, array: this.recipes});
    this.triggerRecipe(index, false);
  }

  private triggerRecipe(index: number = 0, isChecked: boolean = true) {
    this.recipes[index].isChecked = isChecked;
    this.clickedOnCheck(index);
    this.updateSelectedRecipes(index);
  }

  clickedOnCheck(index) {
    if (this.recipes[index].is_pro && !this.is_subscribed) {
      setTimeout(() => {
        this.recipes[index].isChecked = !this.recipes[index].isChecked;
        // open alert
        this.presentConfirm();
      }, 100);
    }
  }

  clickedOnPlanCheck(index) {
    if (this.recipe_group[index].is_pro && !this.is_subscribed) {
      setTimeout(() => {
        this.recipe_group[index].isChecked = !this.recipe_group[index].isChecked;
        // open alert
        this.presentConfirm();
      }, 100);
    }
  }

  updateSelectedRecipes(index) {
    if (this.recipes[index].isChecked) {
      // check is exists
      if (HelperMethods.isExistsInArrayByProperty({
          array: this.selectedRecipes,
          property: 'id',
          value: this.recipes[index].id
        })) {
        // found update it
        this.selectedRecipes[HelperMethods.getIndexOfArray({
          nameOfField: 'id',
          value: this.recipes[index].id,
          array: this.selectedRecipes
        })].isChecked = this.recipes[index].isChecked;
      } else {
        // add
        for(var i = 0; i<this.recipe_group.length; i++){
          this.recipe_group[i].isChecked = false;
        }
        this.selectedPlanRecipes = [];

        this.selectedRecipes.push(this.recipes[index]);
      }
    } else {
      // remove if exists
      this.selectedRecipes.splice(HelperMethods.getIndexOfArray({
        nameOfField: 'id',
        value: this.recipes[index].id,
        array: this.selectedRecipes
      }), 1);
    }
  }

  updateSelectedPlanRecipes(index) {
    if (this.recipe_group[index].isChecked) {

      for(var i = 0; i<this.recipe_group.length;i++){
        if(i != index){
          this.recipe_group[i].isChecked = false;
        }
      }
      // check is exists
      if (HelperMethods.isExistsInArrayByProperty({
          array: this.selectedPlanRecipes,
          property: 'id',
          value: this.recipe_group[index].id
        })) {
        // found update it
        this.selectedPlanRecipes[HelperMethods.getIndexOfArray({
          nameOfField: 'id',
          value: this.recipe_group[index].id,
          array: this.selectedPlanRecipes
        })].isChecked = this.recipe_group[index].isChecked;
      } else {
        // add
        for(var i = 0; i<this.recipes.length; i++){
          this.recipes[i].isChecked = false;
        }
        this.selectedRecipes = [];
        this.selectedPlanRecipes.push(this.recipe_group[index]);
      }
    } else {
      // remove if exists
      this.selectedPlanRecipes.splice(HelperMethods.getIndexOfArray({
        nameOfField: 'id',
        value: this.recipe_group[index].id,
        array: this.selectedPlanRecipes
      }), 1);
    }
  }

  close() {
    this.viewCtrl.dismiss();
  }

  addRecipes() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.userRecipeService.add(this.accessToken, {recipes: this.selectedRecipes}).subscribe(res => {
      loading.dismiss();
      this.close();
    }, err => {
      loading.dismiss();
      HelperMethods.presentToast(err.message, 'error', {
        toastCtrl: this.toastCtrl
      });
    });
  }

  addRecipesPlans() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();

    console.log(this.selectedPlanRecipes);
    this.userRecipeService.getAllByPlan(this.accessToken, {plans: this.selectedPlanRecipes}).subscribe(res => {
      //loading.dismiss();
      console.log(res);
      //this.close();
      this.userRecipeService.add(this.accessToken, {recipes: res}).subscribe(res => {
        loading.dismiss();
        this.close();
      }, err => {
        loading.dismiss();
        HelperMethods.presentToast(err.message, 'error', {
          toastCtrl: this.toastCtrl
        });
      });

    }, err => {
      loading.dismiss();
      HelperMethods.presentToast(err.message, 'error', {
        toastCtrl: this.toastCtrl
      });
    })
  }

  // functions for second section view
  isGroupMealFetched = false;

  fetchGroupMeals(){

    if(!this.isGroupMealFetched){
      // call and fetch results

      this.recipe_group = [];
      this.startLoading();
      this.recipeService.getAllRecipeGroups(this.accessToken).subscribe((recipe_group: any) => {
        console.log(recipe_group);
        for(var i=0; i < recipe_group.data.length; i++) {
          recipe_group.data[i].isChecked = false;
          recipe_group.data[i].isPlan = true;

        }
        this.recipe_group = recipe_group.data;
        this.recipe_group_o = recipe_group.data;

        this.stopLoading();
      }, err => {
        this.stopLoading();
      });


      // make it false when results fetched
      this.isGroupMealFetched = !this.isGroupMealFetched



    }

  }

  showDetailMealModel(mealitem: any) {

    let modal = this.modalCtrl.create('SearchPage', {isMeal: true, mealitem: mealitem});
    modal.onDidDismiss(async data => {
      this.searchedData = data;
      console.log(this.topTab, data);

      if(this.topTab == 'quickstr'){
        // apply bunch of plan
        console.log(this.topTab, data);

        if(this.searchedData){
          if(this.searchedData['isUpdated']){
            this.selectedPlanRecipes = this.searchedData.plan;
            this.addRecipesPlans();
          }
        }


      }else
      if(this.searchedData){
        if (this.searchedData['isUpdated']) {
          this.startLoading();
          try {
            await this.manageSearchedData();
            this.recipeService.getByFilters({
              token: this.accessToken,
              body: this.searchedData
            }).subscribe((res: any) => {
              if (res.data.length > 0) {
                this.recipes = res.data;
              } else {
                HelperMethods.presentToast('No Recipe Found.', 'error', {
                  toastCtrl: this.toastCtrl
                });
              }
              this.stopLoading();
            });
          } catch (e) {
            this.stopLoading();
          }
        }
      }
    });
    modal.present();
  }

  setFilteredItems1(){
    var self = this;
    this.recipes = this.recipes_o.filter((item) => {
      return item.title.toLowerCase().indexOf(self.searchTerm1.toLowerCase()) > -1;
    });
  }

  setFilteredItems2(){
    var self = this;
    this.recipe_group = this.recipe_group_o.filter((item) => {
      return item.name.toLowerCase().indexOf(self.searchTerm2.toLowerCase()) > -1
    });
  }


}
