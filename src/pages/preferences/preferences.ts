import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, Platform} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {DietProvider} from '../../providers/diet/diet';
import {AllergyProvider} from '../../providers/allergy/allergy';
import {IngredientProvider} from '../../providers/ingredient/ingredient';
import {ServingProvider} from '../../providers/serving/serving';
import {UserProvider} from '../../providers/user/user';

// Helpers
import {HelperMethods} from '../../classes/HelperMethods';

/**
 * Generated class for the PreferencesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

interface CommonInterface {
  isLoading: boolean;
  data: Array<any>;
  loadMoreData?: Array<any>
}

interface PreferencesInterface {
  diets: CommonInterface;
  allergies: CommonInterface;
  ingredients: CommonInterface;
  servings: CommonInterface
}

@IonicPage()
@Component({
  selector: 'page-preferences',
  templateUrl: 'preferences.html',
})
export class PreferencesPage {

  accessToken: string = '';

  selectedDietIndex: number = 0;
  selectedServingIndex: number = 0;

  pageInterface: PreferencesInterface = {
    diets: {
      isLoading: true,
      data: []
    },
    allergies: {
      isLoading: true,
      data: []
    },
    ingredients: {
      isLoading: true,
      data: [],
      loadMoreData: []
    },
    servings: {
      isLoading: true,
      data: []
    }
  };

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private authService: AuthProvider,
              private dietsService: DietProvider,
              private allergyService: AllergyProvider,
              private ingredientService: IngredientProvider,
              private servingService: ServingProvider,
              private userService: UserProvider,
              private alertCtrl: AlertController,
              private platform: Platform) {
    this.authService.getData('accessToken').then(accessToken => {
      this.accessToken = accessToken;
      this.init();
    }, err => {
      console.log(err);
    });
    this.platform.ready().then(() => {
      this.platform.pause.subscribe(() => {
        // went backgroud send all data to the server for update
        this.updateAllData();
      });

      // this.platform.resume.subscribe(() => {
      //   console.log('[INFO] App resumed');
      // });
    });
  }

  ionViewWillLeave() {
    this.updateAllData();
  }

  private enableLoading(moduleName: string = null) {
    if (moduleName) {
      return this.pageInterface[moduleName].isLoading = true;
    }
    this.pageInterface.diets.isLoading = true;
    this.pageInterface.allergies.isLoading = true;
  }

  private disableLoading(moduleName: string = null) {
    if (moduleName) {
      return this.pageInterface[moduleName].isLoading = false;
    }
    this.pageInterface.diets.isLoading = false;
    this.pageInterface.allergies.isLoading = false;
  }

  private handleError(err) {
    console.log(err);
    this.disableLoading();
  }

  private async init() {
    this.getDiets().then(diets => {
      this.pageInterface.diets.data = diets;
      this.selectedDietIndex = HelperMethods.getIndexOfArray({
        nameOfField: 'isSelectedByUser',
        value: true,
        array: this.pageInterface.diets.data
      });
      this.disableLoading('diets');
      this.getAllergies().then(allergies => {
        this.pageInterface.allergies.data = allergies;
        this.disableLoading('allergies');
      }, this.handleError);
      this.disableLoading('allergies');
    }, this.handleError);
    this.manageIngredients();
    this.getServings().then(servings => {
      this.pageInterface.servings.data = servings;
      this.selectedServingIndex = HelperMethods.getIndexOfArray({
        nameOfField: 'isSelectedByUser',
        value: true,
        array: this.pageInterface.servings.data
      });
      this.disableLoading('servings');
    }, this.handleError);
  }

  private async manageIngredients() {
    const ingredients = await this.getIngredients();
    const tempIngredients: Array<any> = [];
    const tempIngredientsHidden: Array<any> = ingredients;
    if (ingredients.length >= 10) {
      for (let i = 0; i < ingredients.length; i++) {
        if (ingredients[i].isSelectedByUser) {
          tempIngredients.push(ingredients[i]);
          tempIngredientsHidden.splice(i, 1);
        } else {
          if (tempIngredients.length < 10) {
            tempIngredients.push(ingredients[i]);
            tempIngredientsHidden.splice(i, 1);
          }
        }
      }
      this.pageInterface.ingredients.data = tempIngredients;
      this.pageInterface.ingredients.loadMoreData = tempIngredientsHidden;
    } else {
      this.pageInterface.ingredients.data = ingredients;
    }
    this.disableLoading('ingredients');
  }

  private getDiets(): any {
    return new Promise((resolve, reject) => {
      this.dietsService.getAll(this.accessToken).subscribe((diets: any) => {
        resolve(diets.data);
      }, reject);
    });
  }

  private getAllergies(): any {
    return new Promise((resolve, reject) => {
      this.allergyService.getAllByDiets(this.accessToken, this.pageInterface.diets.data[this.selectedDietIndex].id).subscribe((allergies: any) => {
        resolve(allergies.data);
      }, reject);
    });
  }

  private getIngredients(): any {
    return new Promise((resolve, reject) => {
      this.ingredientService.getForAll(this.accessToken).subscribe((ingredients: any) => {
        resolve(ingredients.data);
      }, reject);
    });
  }

  private getServings(): any {
    return new Promise((resolve, reject) => {
      this.servingService.getAllWithAuth(this.accessToken).subscribe((servings: any) => {
        resolve(servings.data);
      }, reject);
    });
  }

  loadMoreIngredients() {
    let alert = this.alertCtrl.create({
      enableBackdropDismiss: false
    });
    alert.setTitle('Add Ingredients you dislike');
    for (let dislike of this.pageInterface.ingredients.loadMoreData) {
      alert.addInput({
        type: 'checkbox',
        label: dislike.name,
        value: dislike,
        checked: false
      });
    }
    alert.addButton({
      text: 'Add',
      handler: selectedDislike => {
        if (selectedDislike && selectedDislike.length > 0) {
          for (const dislike of selectedDislike) {
            const index = HelperMethods.getIndexOfArray({
              nameOfField: 'id',
              value: dislike.id,
              array: this.pageInterface.ingredients.loadMoreData
            });
            dislike.isSelectedByUser = true;
            this.pageInterface.ingredients.data.push(dislike);
            this.pageInterface.ingredients.loadMoreData.splice(index, 1);
          }
        }
      }
    });
    alert.present();
  }

  dietUpdated() {
    this.enableLoading('allergies');
    this.getAllergies().then(allergies => {
      this.pageInterface.allergies.data = allergies;
      this.pageInterface.allergies.isLoading = false;
    }, () => this.disableLoading('allergies'));
  }

  private generateData() {
    const allergies: Array<any> = [];
    for (let allergy of this.pageInterface.allergies.data) {
      if (allergy.isSelectedByUser) {
        allergies.push(allergy.id);
      }
    }
    const dislikes: Array<any> = [];
    for (let ingredient of this.pageInterface.ingredients.data) {
      if (ingredient.isSelectedByUser) {
        dislikes.push(ingredient.id);
      }
    }
    return {
      diets: this.pageInterface.diets.data[this.selectedDietIndex].id,
      allergies,
      dislikes,
      servings: this.pageInterface.servings.data[this.selectedServingIndex].id
    };
  }

  private updateAllData() {
    const data = this.generateData();
    this.userService.update(data, this.accessToken).subscribe();
  }

}
