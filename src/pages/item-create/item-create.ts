import {Component} from '@angular/core';
import {Events, IonicPage, ModalController, NavController} from 'ionic-angular';
import {Storage} from '@ionic/storage';

// Providers
import {AuthProvider} from "../../providers/auth/auth";
import {IngredientProvider} from '../../providers/ingredient/ingredient';
import {RecipeIngredientProvider} from "../../providers/recipe-ingredient/recipe-ingredient";

// Helper Methods
import {HelperMethods} from "../../classes/HelperMethods";

@IonicPage()
@Component({
  selector: 'page-item-create',
  templateUrl: 'item-create.html'
})
export class ItemCreatePage {

  accessToken: string = '';
  isLoading: boolean = true;

  realIngredientData: any;
  ingredients: any = {};

  getIngredientQuantityAndUnitHTML: any = HelperMethods.getIngredientQuantityAndUnitHTML;

  showItemCompleted: boolean = false;

  toggleCompletedItemsShow: boolean = false;

  totalCompleteItems: number = 0;

  completedIngredients: Array<any> = [];

  constructor(private navCtrl: NavController,
              private authService: AuthProvider,
              private recipeIngredientService: RecipeIngredientProvider,
              private modalCtrl: ModalController,
              private events: Events,
              private storage: Storage,
              private ingredientService: IngredientProvider) {
    this.events.subscribe('Ingredient:get', this.getIngredients.bind(this));
    this.authService.getData('accessToken').then(accessToken => {
      this.accessToken = accessToken;
      this.events.publish('Ingredient:get');
    });
  }











  private getIngredients() {
    return new Promise((resolve, reject) => {
      this.recipeIngredientService.getAll(this.accessToken).subscribe((res: any) => {
        // this.ingredients = res.data;
        console.log(res.data);
        this.realIngredientData = res.data;
        this.ingredientService.manageIngredientsData({
          realData: res.data,
          isCatCompleted: ItemCreatePage.isCatCompleted,
          isShowCompleteItem: this.isShowCompleteItem.bind(this)
        }).then(ingredients => {
          this.ingredients = ingredients;
          this.stopLoading();
          resolve();
        });
      }, err => {
        reject(err);
      });
    });
  }

  private stopLoading() {
    this.isLoading = false;
  }

  iterateIngredientTypeTitle(): Array<string> {
    return Object.keys(this.ingredients);
  }

  add() {
    let modal = this.modalCtrl.create('AddIngredientPage');
    modal.onDidDismiss((data: any) => {
      if (data.isLoading) {
        this.events.publish('Ingredient:get');
        data.loading.dismiss();
      }
    });
    modal.present();
  }

  static isCatCompleted(ingredients, title) {
    let isCompleted = [];
    for (let ingredient of ingredients[title].data) {
      if (ingredient.isChecked) {
        isCompleted.push(ingredient);
      }
    }
    ingredients[title].isCompleted = isCompleted.length === ingredients[title].data.length;
    return ingredients[title].isCompleted;
  }

  private isShowCompleteItem(ingredients) {
    this.showItemCompleted = false;
    this.totalCompleteItems = 0;
    // for (let title of this.iterateIngredientTypeTitle()) {
    for (let title in ingredients) {
      for (let ingredient of ingredients[title].data) {
        if (ingredient.isChecked) {
          this.showItemCompleted = true;
          this.totalCompleteItems++;
        }
      }
    }
  }

  updateCompletedIngredients(title, index) {
    if (this.ingredients[title].data[index].isChecked) {
      // remove
      this.events.publish('Ingredient:remove');
    } else {
      // add
      this.events.publish('Ingredient:add');
    }
    // check if specific category is completed or not
    this.ingredients[title].isCompleted = ItemCreatePage.isCatCompleted(this.ingredients, title);

    this.isShowCompleteItem(this.ingredients);

    // storage
    this.manageLocalStorage(title, index);
  }

  private removeFromLocalStorage(title, index) {
    this.storage.get('completedIngredients').then(res => {
      if (res) {
        if (HelperMethods.isExistsInArrayByProperty({
            array: res,
            property: 'id',
            value: this.ingredients[title].data[index].id
          })) {
          res.splice(HelperMethods.getIndexOfArray({
            nameOfField: 'id',
            value: this.ingredients[title].data[index].id,
            array: res
          }), 1);
          this.storage.remove('completedIngredients');
          this.storage.set('completedIngredients', res);
        }
      }
    });
  }

  private addUpdateInLocalStorage(title, index) {
    this.storage.get('completedIngredients').then(res => {
      if (res) {
        if (HelperMethods.isExistsInArrayByProperty({
            array: res,
            property: 'id',
            value: this.ingredients[title].data[index].id
          })) {
          res.splice(HelperMethods.getIndexOfArray({
            nameOfField: 'id',
            value: this.ingredients[title].data[index].id,
            array: res
          }), 1);
          this.addUpdateInLocalStorage(title, index);
        } else {
          // add it here
          res.push(this.ingredients[title].data[index]);
          this.storage.set('completedIngredients', res);
        }
      } else {
        this.storage.set('completedIngredients', [this.ingredients[title].data[index]]);
      }
    });
  }

  private manageLocalStorage(title, index) {
    if (this.ingredients[title].data[index].isChecked) {
      // add in storage if not exists or if exists update it
      this.addUpdateInLocalStorage(title, index);
    } else {
      // remove from storage if exists
      this.removeFromLocalStorage(title, index);
    }
  }

  toggleCompletedItems() {
    this.toggleCompletedItemsShow = !this.toggleCompletedItemsShow;
  }

}
