import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MealplanPage } from './mealplan';

@NgModule({
  declarations: [
    MealplanPage,
  ],
  imports: [
    IonicPageModule.forChild(MealplanPage),
  ],
})
export class MealplanPageModule {}
