import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, Events} from 'ionic-angular';
import {ModalController} from 'ionic-angular';

// Provider
import {AuthProvider} from '../../providers/auth/auth';
import {UserProvider} from '../../providers/user/user';
import {UserRecipeProvider} from '../../providers/user-recipe/user-recipe';
import {FavouriteProvider} from '../../providers/favourite/favourite';

/**
 * Generated class for the MealplanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mealplan',
  templateUrl: 'mealplan.html',
})
export class MealplanPage {

  accessToken: string = '';
  isLoading: boolean = false;

  recipes: Array<any> = [];

  isFavourite: boolean = false;
  title: string = 'Meal Plan';
  defaultImagePath: string = 'assets/img/welcome_optimized.png';

  isPrevious: boolean = false;
  previousMealPlanData: { data: Array<any>, isShow: boolean, isLoading: boolean } = {
    data: [],
    isShow: false,
    isLoading: true
  };

  is_subscribed: boolean = false;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private modalCtrl: ModalController,
              private authService: AuthProvider,
              private userService: UserProvider,
              private userRecipeService: UserRecipeProvider,
              private favouriteService: FavouriteProvider,
              private events: Events) {
    this.userService.isSubscribed().then((is_subscribed: boolean) => (this.is_subscribed = is_subscribed));
  }

  ionViewWillEnter() {
    this.isFavourite = this.navParams.get('isFavourite');
    if (this.isFavourite) {
      this.title = 'Favourite';
      this.defaultImagePath = 'assets/img/favourite_optimized.png';
    }
    this.init();
  }

  private init() {
    return new Promise(resolve => {
      this.authService.getData('accessToken').then(accessToken => {
        this.accessToken = accessToken;
        this.getRecipes().then(resolve).catch(resolve);
      });
    });
  }

  private startLoading() {
    this.isLoading = true;
  }

  private stopLoading() {
    this.isLoading = false;
  }

  getRecipes() {
    return new Promise(async (resolve, reject) => {
      this[(this.isFavourite) ? 'favouriteService' : 'userRecipeService']
        .getAll(this.accessToken)
        .subscribe((recipes: any) => {
          this.recipes = recipes.data;

          this.recipes.forEach((rec) =>{
            rec.image = (rec.image.match(/\.(jpeg|jpg|gif|png)$/) != null) ? rec.image : 'assets/img/default_recipe.jpg';
          });

          if (!this.isFavourite && this.is_subscribed) {
            this.isPrevious = recipes.isPrevious;
            if (this.isPrevious) {
              this.getPreviousRecipes();
            }
          }
          resolve();
        }, err => {
          reject(err);
        });
    });
  }

  private getPreviousRecipes() {
    this.userRecipeService.getPreviousMealPlan(this.accessToken).subscribe((recipes: any) => {
      // this.previousMealPlanData.data = recipes.data;
      this.previousMealPlanData.isLoading = false;
    }, err => {
      console.log(err);
    });
  }

  openRecipe(recipe) {
    console.log(recipe);
    console.log("print");
    this.navCtrl.push('ItemDetailPage', {
      recipe: recipe,
      added: true
    });
  }

  presentModal() {
    this.startLoading();
    let modal = this.modalCtrl.create('ListMasterPage');
    modal.onDidDismiss(() => {
      this.getRecipes().then(() => {
        this.events.publish('Ingredient:get');
        this.stopLoading();
      }).catch(err => {
        this.stopLoading();
      });
    });
    modal.present();
  }

  togglePreviousMealPlan() {
    this.previousMealPlanData.isShow = !this.previousMealPlanData.isShow;
  }

  openSubscriptionModal() {
    let modal = this.modalCtrl.create('UpgradePage');
    modal.onDidDismiss(async data => {

    });
    modal.present();
  }

}
