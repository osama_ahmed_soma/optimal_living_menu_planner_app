import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController, LoadingController} from 'ionic-angular';

// Providers
import {AuthProvider} from "../../providers/auth/auth";
import {IngredientTypeProvider} from '../../providers/ingredient-type/ingredient-type';
import {UnitProvider} from '../../providers/unit/unit';
import {IngredientProvider} from '../../providers/ingredient/ingredient';
import {UserIngredientProvider} from '../../providers/user-ingredient/user-ingredient';

/**
 * Generated class for the AddIngredientPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-ingredient',
  templateUrl: 'add-ingredient.html',
})
export class AddIngredientPage {

  accessToken: string = '';
  ingredientData: {
    name: string,
    type: number,
    quantity: number,
    unit: number,
    subQuantity: number,
    subUnit: number
  } = {
    name: '',
    type: null,
    quantity: null,
    unit: null,
    subQuantity: null,
    subUnit: null
  };
  isLoading: boolean = true;
  isSubUnit: boolean = false;

  ingredientTypes: Array<any> = [];
  units: Array<any> = [];

  errors: {
    name: string,
    type: string,
    quantity: string,
    unit: string,
    subQuantity: string,
    subUnit: string
  } = {
    name: '',
    type: '',
    quantity: '',
    unit: '',
    subQuantity: '',
    subUnit: ''
  };

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private viewCtrl: ViewController,
              private authService: AuthProvider,
              private ingredientTypeService: IngredientTypeProvider,
              private unitService: UnitProvider,
              private ingredientService: IngredientProvider,
              private userIngredientService: UserIngredientProvider,
              private loadingCtrl: LoadingController) {
    // get all types
    this.authService.getData('accessToken').then(accessToken => {
      this.accessToken = accessToken;
      this.init().then(this.stopLoading.bind(this));
    });
  }

  private init() {
    return new Promise(async resolve => {
      await this.setIngredientTypes();
      await this.setUnits();
      resolve();
    });
  }

  private setIngredientTypes() {
    return new Promise(resolve => {
      this.ingredientTypeService.getAll(this.accessToken).subscribe((ingredientTypes: any) => {
        this.ingredientTypes = ingredientTypes.data;
        this.ingredientData.type = this.ingredientTypes[0].id;
        resolve();
      }, err => {
        resolve();
      });
    });
  }

  private setUnits() {
    return new Promise(resolve => {
      this.unitService.getAll(this.accessToken).subscribe((res: any) => {
        this.units = res.data;
        this.ingredientData.unit = this.units[0].id;
        this.ingredientData.subUnit = this.units[0].id;
        resolve();
      }, err => resolve());
    });
  }

  async addIngredient() {
    this.setErrors({});
    if (!this.ingredientData.name.trim()) {
      this.setError('name', 'Name field is required.');
    }
    if (!this.ingredientData.quantity) {
      this.setError('quantity', 'Quantity field is required and must be number or decimal number.');
    }
    if (!(await this.checkIsError())) {
      // proceed
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });

      loading.present();
      this.userIngredientService.add(this.accessToken, this.ingredientData).subscribe((res: any) => {
        this.close({
          loading,
          isLoading: true,
          data: res.data
        });
      });
    }
  }

  private checkIsError() {
    return new Promise(resolve => {
      for (const key in this.errors) {
        if (this.errors[key].trim()) {
          resolve(true);
        }
      }
      resolve(false);
    });
  }

  private setError(key, value) {
    this.errors[key] = value;
  }

  private setErrors({name = '', type = '', quantity = '', unit = '', subQuantity = '', subUnit = ''}) {
    this.errors = {
      name,
      type,
      quantity,
      unit,
      subQuantity,
      subUnit
    };
  }

  private startLoading() {
    this.isLoading = true;
  }

  private stopLoading() {
    this.isLoading = false;
  }

  close(data: { loading?: any, isLoading: boolean, data?: Array<any> } = {isLoading: false}) {
    this.viewCtrl.dismiss(data);
  }

}
