import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubscriptionSuccessPage } from './subscription-success';

@NgModule({
  declarations: [
    SubscriptionSuccessPage,
  ],
  imports: [
    IonicPageModule.forChild(SubscriptionSuccessPage),
  ],
})
export class SubscriptionSuccessPageModule {}
