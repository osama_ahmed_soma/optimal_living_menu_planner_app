import {Component} from '@angular/core';
import {IonicPage, ViewController} from 'ionic-angular';

/**
 * Generated class for the SubscriptionSuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subscription-success',
  templateUrl: 'subscription-success.html',
})
export class SubscriptionSuccessPage {

  constructor(private viewCtrl: ViewController,) {
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
