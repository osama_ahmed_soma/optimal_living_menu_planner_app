import {Component} from '@angular/core';
import {IonicPage, NavParams} from 'ionic-angular';

/**
 * Generated class for the CookWareContentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cook-ware-content',
  templateUrl: 'cook-ware-content.html',
})
export class CookWareContentPage {

  cookware: any;

  constructor(private navParams: NavParams) {
    this.cookware = this.navParams.get('cookware');
  }

}
