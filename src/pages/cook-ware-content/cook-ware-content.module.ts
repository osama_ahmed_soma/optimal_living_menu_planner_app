import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CookWareContentPage } from './cook-ware-content';

@NgModule({
  declarations: [
    CookWareContentPage,
  ],
  imports: [
    IonicPageModule.forChild(CookWareContentPage),
  ],
})
export class CookWareContentPageModule {}
