import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, ModalController, Events} from 'ionic-angular';
import {PreferencesPage} from "../preferences/preferences";
import {AccountPage} from "../account/account";

// Providers
import {UserProvider} from '../../providers/user/user';

/**
 * The Settings page is a simple form that syncs with a Settings provider
 * to enable the user to customize settings for the app.
 *
 */
@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  is_subscribed: boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              private modalCtrl: ModalController,
              private events: Events,
              private userService: UserProvider) {
    this.userService.isSubscribed().then((is_subscribed: boolean) => (this.is_subscribed = is_subscribed));
  }

  openUpgrade() {
    let modal = this.modalCtrl.create('UpgradePage');
    modal.onDidDismiss(async data => {

    });
    modal.present();
  }

  openPreferences() {
    this.navCtrl.push('PreferencesPage');
  }

  openAccount() {
    this.navCtrl.push('AccountPage');
  }

  logout() {
    this.events.publish('user:logout');
  }

}
