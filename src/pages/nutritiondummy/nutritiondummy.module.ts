import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NutritiondummyPage } from './nutritiondummy';

@NgModule({
  declarations: [
    NutritiondummyPage,
  ],
  imports: [
    IonicPageModule.forChild(NutritiondummyPage),
  ],
})
export class NutritiondummyPageModule {}
