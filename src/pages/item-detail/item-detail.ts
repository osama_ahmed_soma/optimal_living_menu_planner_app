import {Component} from '@angular/core';
import { Clipboard } from '@ionic-native/clipboard';
import { Platform } from 'ionic-angular';
import {
  IonicPage, NavController, NavParams, ViewController, AlertController, Events,
  ModalController
} from 'ionic-angular';
import {isArray} from "ionic-angular/es2015/util/util";
import {Storage} from '@ionic/storage';

// Helper Methods
import {HelperMethods} from "../../classes/HelperMethods";

//Providers
import {AuthProvider} from '../../providers/auth/auth';
import {UserProvider} from '../../providers/user/user';
import {RecipeProvider} from '../../providers/recipe/recipe';
import {FavouriteProvider} from "../../providers/favourite/favourite";
import {CookWareContentPage} from "../cook-ware-content/cook-ware-content";
import {NotesPage} from "../notes/notes";
import {NutritionPage} from "../nutrition/nutrition";

@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html'
})
export class ItemDetailPage {

  accessToken: string = '';

  recipe: any;
  isLoading: boolean = true;

  segments: string = 'Ingredients';

  getIngredientQuantityAndUnitHTML: any = HelperMethods.getIngredientQuantityAndUnitHTML;

  getIngredientQuantityAndUnitHTMLViaServing: any = HelperMethods.getIngredientQuantityAndUnitHTMLViaServing;

  isDirect: boolean = false;

  footerFakeTabsConfig: any = {
    share: {
      color: 'white'
    },
    add_note: {
      color: 'white'
    },
    favourite: {
      color: 'white'
    },
    complete: {
      color: 'white'
    }
  };

  serving: number = 0;

  isAddedToMealPlan: boolean = false;

  is_subscribed: boolean = false;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private viewCtrl: ViewController,
              private authService: AuthProvider,
              private userService: UserProvider,
              private recipeService: RecipeProvider,
              private alertCtrl: AlertController,
              private favouriteService: FavouriteProvider,
              private storage: Storage,
              private events: Events,
              private modalCtrl: ModalController,
              private clipboard: Clipboard,
              public plt: Platform) {

    //console.log(this.navParams.get('recipe'));
    this.userService.isSubscribed().then((is_subscribed: boolean) => (this.is_subscribed = is_subscribed));
    this.isDirect = (this.navParams.get('added')) ? this.navParams.get('added') : false;

    var recipe = this.navParams.get('recipe');

    if (recipe.hasOwnProperty('isChecked') && recipe.isChecked) {
      this.isAddedToMealPlan = true;
    }
    this.recipe = {
      units: [],
      recipe,
      dish_types: [],
      cookwares: [],
      prep_instructions: [],
      ingredients: []
    };
    // get specific recipe
    //console.log(this.recipe);
    this.authService.getData('accessToken').then(accessToken => {
      this.accessToken = accessToken;

      this.recipeService.get({
        recipe_id: this.recipe.recipe.id,
        token: this.accessToken
      }).subscribe((reci: any) => {
        console.log(reci);
        this.manageRecipeData(reci);
        this.isLoading = false;
      });
    });




    // // get user serving from local storage
    // this.storage.ready().then(async () => {
    //   const user = await this.storage.get('menu_planner_user_data');
    //   this.serving = user.serving.value;
    // });
  }

  private manageRecipeData(reci) {
    if (!isArray(reci.data.prep_instructions)) {
      reci.data.prep_instructions = [reci.data.prep_instructions];
    }
    this.recipe = reci.data;
    this.activeNotes();
    if (this.isDirect && this.recipe.isFavourite) {
      this.footerFakeTabsConfig.favourite.color = 'buttonSelected';
    } else {
      this.footerFakeTabsConfig.favourite.color = 'white';
    }
  }

  private activeNotes() {
    if(this.recipe.notes){
      this.footerFakeTabsConfig.add_note.color = this.recipe.notes.trim() ? 'buttonSelected' : 'white';
    }else{
      this.footerFakeTabsConfig.add_note.color = "white";
    }

    // // get user serving from server
    this.authService.getData('accessToken').then(accessToken => {
      this.accessToken = accessToken;

      console.log(this.recipe.id)
      this.recipeService.getServingQuantity({
        recipe_id: this.recipe.id,
        token: this.accessToken
      }).subscribe((v: any) => {
        this.serving = v;
        this.isLoading = false;
      });
    });

  }

  radioChecked(qty){
    console.log(qty);
    this.authService.getData('accessToken').then(accessToken => {
      this.accessToken = accessToken;

      this.recipeService.postServingQuantity({
        serving: qty,
        recipe_id: this.recipe.id,
        token: this.accessToken
      }).subscribe((reci: any) => {
        console.log(reci);
        this.events.publish('Ingredient:get');
        this.isLoading = false;
      });
    });
  }

  openCookWare(cookware) {
    this.navCtrl.push('CookWareContentPage', {
      cookware
    });
  }

  share() {

    // simulate a network phase here to fetch exact link
    var plink = "r.olmp.com/456545667";
    if (this.plt.is('ios')) {
      // This will only print when on iOS
      this.clipboard.copy(plink);
      let alert = this.alertCtrl.create(HelperMethods.generateShareAlertOptions(plink, null));
      alert.present();
    }else{
      console.log("platform not supported");

    }



  }

  showNutritionInfo(ingr: any){

    // comment out subscribed for now
    //if (this.is_subscribed) {
      // open modal




      let modal = this.modalCtrl.create('NutritionPage', {
        title: this.recipe.title,
        ingr: ingr
      });
      modal.onDidDismiss(async data => {
        console.log(data);
      });
      modal.present();

    // end part of subscribe (un-comment when done)
    //   return;
    // }
    // // show alert
    // this.presentConfirm();
  }

  addNote() {
    if (this.is_subscribed) {
      // open modal
      let modal = this.modalCtrl.create('NotesPage', {
        recipeID: this.recipe.id,
        notes: this.recipe.notes ? this.recipe.notes : ''
      });
      modal.onDidDismiss(async data => {
        console.log(data);
        if(data.notes){
          this.recipe.notes = data.notes;
          this.activeNotes();
        }


      });
      modal.present();
      return;
    }
    // show alert
    this.presentConfirm();
  }

  private presentConfirm() {
    let alert = this.alertCtrl.create(HelperMethods.generateSubscriptionAlertOptions(() => {
      let modal = this.modalCtrl.create('UpgradePage');
      modal.onDidDismiss(async data => {

      });
      modal.present();
    }, null));
    alert.present();
  }

  favorite() {
    if (!this.recipe.isFavourite) {
      let alert = this.alertCtrl.create({
        title: 'Yum! This recipe has been added to your favourites.',
        // subTitle: '10% of battery remaining',
        buttons: ['Ok']
      });
      alert.present();
      this.favouriteService.add(this.accessToken, {
        recipeID: this.recipe.id
      }).subscribe(recipe => {
        this.manageRecipeData(recipe);
      }, err => {
        console.log(err);
      });
    } else {
      let alert = this.alertCtrl.create({
        title: 'This recipe has been removed to your favourites.',
        buttons: ['Ok']
      });
      alert.present();
      this.favouriteService.remove(this.accessToken, this.recipe.id).subscribe(recipe => {
        this.manageRecipeData(recipe);
      }, err => {
        console.log(err);
      });
    }
  }

  complete() {

  }

  addToMeanPlan() {
    this.navCtrl.pop({}, () => {
      this.events.publish('Recipe:Add', this.recipe);
    });
  }

  removeFromMeanPlan() {
    this.navCtrl.pop({}, () => {
      this.events.publish('Recipe:Remove', this.recipe);
    });
  }

}
