import {Component, ViewChild} from '@angular/core';
import {IonicPage, ViewController, NavParams, LoadingController} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {RecipeProvider} from '../../providers/recipe/recipe';
import {UserRecipeProvider} from '../../providers/user-recipe/user-recipe';

import { PipeTransform, Pipe } from '@angular/core';
@Pipe({name: 'keys'})
export class KeysPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    let keys = [];
    for (let key in value) {
      keys.push(key);
    }
    return keys;
  }
}
/**
 * Generated class for the NutritionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nutritions',
  templateUrl: 'nutrition.html',
})
export class NutritionPage {

  @ViewChild('areaToFocus') areaToFocus;

  accessToken: string = '';

  isLoading: boolean = true;

  title: string = '';
  ingr: Array<string> = [];
  actualNotes: string = null;

  totalNutrantKeys: any;
  totalDailyKeys: any;


  bigdata = {
    "uri": "http://www.edamam.com/ontologies/edamam.owl#recipe_e5b9edffc7e940f7bee8e8f784b51191",
    "yield": 15,
    "calories": 18491,
    "totalWeight": 8033.449795779999,
    "dietLabels": [
        "LOW_CARB"
    ],
    "healthLabels": [
        "PEANUT_FREE",
        "TREE_NUT_FREE",
        "ALCOHOL_FREE"
    ],
    "cautions": [],
    "totalNutrients": {
        "ENERC_KCAL": {
            "label": "Energy",
            "quantity": 18491.0627869398,
            "unit": "kcal"
        },
        "FAT": {
            "label": "Fat",
            "quantity": 1300.8028115600077,
            "unit": "g"
        },
        "FASAT": {
            "label": "Saturated",
            "quantity": 447.8895281588015,
            "unit": "g"
        },
        "FATRN": {
            "label": "Trans",
            "quantity": 0.01634,
            "unit": "g"
        },
        "FAMS": {
            "label": "Monounsaturated",
            "quantity": 577.6090502868124,
            "unit": "g"
        },
        "FAPU": {
            "label": "Polyunsaturated",
            "quantity": 140.3907744408204,
            "unit": "g"
        },
        "CHOCDF": {
            "label": "Carbs",
            "quantity": 409.15387441321,
            "unit": "g"
        },
        "FIBTG": {
            "label": "Fiber",
            "quantity": 56.39288635894,
            "unit": "g"
        },
        "SUGAR": {
            "label": "Sugars",
            "quantity": 205.96848064307204,
            "unit": "g"
        },
        "PROCNT": {
            "label": "Protein",
            "quantity": 1226.943442330662,
            "unit": "g"
        },
        "CHOLE": {
            "label": "Cholesterol",
            "quantity": 5114.129105694,
            "unit": "mg"
        },
        "NA": {
            "label": "Sodium",
            "quantity": 14086.833861262,
            "unit": "mg"
        },
        "CA": {
            "label": "Calcium",
            "quantity": 948.0259611413999,
            "unit": "mg"
        },
        "MG": {
            "label": "Magnesium",
            "quantity": 1681.6064390057998,
            "unit": "mg"
        },
        "K": {
            "label": "Potassium",
            "quantity": 26425.273906824194,
            "unit": "mg"
        },
        "FE": {
            "label": "Iron",
            "quantity": 75.55521247915802,
            "unit": "mg"
        },
        "ZN": {
            "label": "Zinc",
            "quantity": 136.713840627502,
            "unit": "mg"
        },
        "P": {
            "label": "Phosphorus",
            "quantity": 14313.2173805304,
            "unit": "mg"
        },
        "VITA_RAE": {
            "label": "Vitamin A",
            "quantity": 469.6835427546,
            "unit": "µg"
        },
        "VITC": {
            "label": "Vitamin C",
            "quantity": 68.0188900546,
            "unit": "mg"
        },
        "THIA": {
            "label": "Thiamin (B1)",
            "quantity": 51.13441042842641,
            "unit": "mg"
        },
        "RIBF": {
            "label": "Riboflavin (B2)",
            "quantity": 15.304394633964002,
            "unit": "mg"
        },
        "NIA": {
            "label": "Niacin (B3)",
            "quantity": 330.5154361333834,
            "unit": "mg"
        },
        "VITB6A": {
            "label": "Vitamin B6",
            "quantity": 28.794593914299806,
            "unit": "mg"
        },
        "FOLDFE": {
            "label": "Folate equivalent (total)",
            "quantity": 849.9634645026,
            "unit": "µg"
        },
        "FOLFD": {
            "label": "Folate (food)",
            "quantity": 664.1434645026,
            "unit": "µg"
        },
        "FOLAC": {
            "label": "Folic acid",
            "quantity": 113.28,
            "unit": "µg"
        },
        "VITB12": {
            "label": "Vitamin B12",
            "quantity": 43.07572104914,
            "unit": "µg"
        },
        "VITD": {
            "label": "Vitamin D",
            "quantity": 34.743350039,
            "unit": "µg"
        },
        "TOCPHA": {
            "label": "Vitamin E",
            "quantity": 10.644519794991997,
            "unit": "mg"
        },
        "VITK1": {
            "label": "Vitamin K",
            "quantity": 193.59131292326,
            "unit": "µg"
        }
    },
    "totalDaily": {
        "ENERC_KCAL": {
            "label": "Energy",
            "quantity": 924.5531393469901,
            "unit": "%"
        },
        "FAT": {
            "label": "Fat",
            "quantity": 2001.235094707704,
            "unit": "%"
        },
        "FASAT": {
            "label": "Saturated",
            "quantity": 2239.4476407940074,
            "unit": "%"
        },
        "CHOCDF": {
            "label": "Carbs",
            "quantity": 136.38462480440333,
            "unit": "%"
        },
        "FIBTG": {
            "label": "Fiber",
            "quantity": 225.57154543576,
            "unit": "%"
        },
        "PROCNT": {
            "label": "Protein",
            "quantity": 2453.886884661324,
            "unit": "%"
        },
        "CHOLE": {
            "label": "Cholesterol",
            "quantity": 1704.709701898,
            "unit": "%"
        },
        "NA": {
            "label": "Sodium",
            "quantity": 586.9514108859166,
            "unit": "%"
        },
        "CA": {
            "label": "Calcium",
            "quantity": 94.80259611413999,
            "unit": "%"
        },
        "MG": {
            "label": "Magnesium",
            "quantity": 420.40160975144994,
            "unit": "%"
        },
        "K": {
            "label": "Potassium",
            "quantity": 755.0078259092627,
            "unit": "%"
        },
        "FE": {
            "label": "Iron",
            "quantity": 419.75118043976676,
            "unit": "%"
        },
        "ZN": {
            "label": "Zinc",
            "quantity": 911.4256041833468,
            "unit": "%"
        },
        "P": {
            "label": "Phosphorus",
            "quantity": 2044.7453400757713,
            "unit": "%"
        },
        "VITA_RAE": {
            "label": "Vitamin A",
            "quantity": 52.18706030606666,
            "unit": "%"
        },
        "VITC": {
            "label": "Vitamin C",
            "quantity": 113.36481675766667,
            "unit": "%"
        },
        "THIA": {
            "label": "Thiamin (B1)",
            "quantity": 3408.960695228428,
            "unit": "%"
        },
        "RIBF": {
            "label": "Riboflavin (B2)",
            "quantity": 900.2585078802355,
            "unit": "%"
        },
        "NIA": {
            "label": "Niacin (B3)",
            "quantity": 1652.577180666917,
            "unit": "%"
        },
        "VITB6A": {
            "label": "Vitamin B6",
            "quantity": 1439.7296957149904,
            "unit": "%"
        },
        "FOLDFE": {
            "label": "Folate equivalent (total)",
            "quantity": 212.49086612565,
            "unit": "%"
        },
        "VITB12": {
            "label": "Vitamin B12",
            "quantity": 717.9286841523334,
            "unit": "%"
        },
        "VITD": {
            "label": "Vitamin D",
            "quantity": 8.68583750975,
            "unit": "%"
        },
        "TOCPHA": {
            "label": "Vitamin E",
            "quantity": 53.222598974959986,
            "unit": "%"
        },
        "VITK1": {
            "label": "Vitamin K",
            "quantity": 241.989141154075,
            "unit": "%"
        }
    }
}

  constructor(private viewCtrl: ViewController,
              private navParams: NavParams,
              private userRecipeService: UserRecipeProvider,
              private authService: AuthProvider,
              private recipeService: RecipeProvider,
              private loadingCtrl: LoadingController) {
    this.title = this.navParams.get('title');
    var ing = this.navParams.get('ingr');
    for(var i = 0; i < ing.length; i++){
      var str: string = ing[i]["quantity"] + " " + ing[i]["unit"]["name"] + " " + ing[i]["name"];
      this.ingr.push(str);
    }

    var dump = {
      "title": this.title,
      "yield": "About 1 servings",
      "ingr": this.ingr
    }

    this.recipeService.getNutritionInfoByApi({
      data: dump
    }).subscribe((reci: any) => {
      console.log(reci);
      this.bigdata = reci;
      this.totalNutrantKeys = Object.keys(this.bigdata["totalNutrients"]);
      this.totalDailyKeys = Object.keys(this.bigdata["totalDaily"]);

      this.isLoading = false;

    });







    this.authService.getData('accessToken').then(accessToken => (this.accessToken = accessToken));
  }

  ionViewDidEnter() {

  }



  close(isRefresh: boolean = false) {
    this.viewCtrl.dismiss();
  }

}
