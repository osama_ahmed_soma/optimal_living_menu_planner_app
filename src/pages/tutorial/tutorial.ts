import { Component, ViewChild } from '@angular/core';
import { IonicPage, MenuController, NavController, Platform, Slides } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage {

  @ViewChild(Slides) slides: Slides;
  showSkip = true;
  dir: string = 'ltr';

  constructor(public navCtrl: NavController, public menu: MenuController, public platform: Platform) {
  }

  goToNextSlide(){
    const isEnd = this.slides.isEnd();
    this.slides.slideNext();
    if(isEnd){
      this.openPreferredDiet();
    }
  }

  openPreferredDiet(){
    this.navCtrl.setRoot('PreferredDietPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd();
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

  openLoginPage() {
    this.navCtrl.push('LoginPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }
}
