import {Component} from '@angular/core';
import {IonicPage, NavParams} from 'ionic-angular';

/**
 * Generated class for the DishTypeContentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dish-type-content',
  templateUrl: 'dish-type-content.html',
})
export class DishTypeContentPage {

  dishType: any;

  constructor(private navParams: NavParams) {
    this.dishType = this.navParams.get('dishType');
  }

}
