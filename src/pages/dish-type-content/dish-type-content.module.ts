import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DishTypeContentPage } from './dish-type-content';

@NgModule({
  declarations: [
    DishTypeContentPage,
  ],
  imports: [
    IonicPageModule.forChild(DishTypeContentPage),
  ],
})
export class DishTypeContentPageModule {}
