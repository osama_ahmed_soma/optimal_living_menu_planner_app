import {Component} from '@angular/core';
import {IonicPage, Events} from 'ionic-angular';

// Providers
import {AuthProvider} from "../../providers/auth/auth";
import {RecipeIngredientProvider} from "../../providers/recipe-ingredient/recipe-ingredient";
import {IngredientProvider} from "../../providers/ingredient/ingredient";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  accessToken: string = '';
  totalIngredients: number = 0;

  constructor(private authService: AuthProvider,
              private recipeIngredientService: RecipeIngredientProvider,
              private events: Events,
              private ingredientService: IngredientProvider) {
    this.events.subscribe('Ingredient:get:first:time', this.getIngredients.bind(this));
    this.events.subscribe('Ingredient:remove', this.removeIngredient.bind(this));
    this.events.subscribe('Ingredient:add', this.addIngredient.bind(this));

    this.events.subscribe('Ingredient:total', this.setTotalIngredient.bind(this));

    this.events.publish('Ingredient:get:first:time');
  }

  private addIngredient() {
    this.totalIngredients++;
  }

  private removeIngredient() {
    this.totalIngredients--;
  }

  private setTotalIngredient(total) {
    this.totalIngredients = total;
  }

  private getIngredients() {
    if (this.accessToken) {
      this.recipeIngredientService.getAll(this.accessToken).subscribe((res: any) => {

        console.log(res);

        this.ingredientService.manageIngredientsData({
          realData: res.data,
          isCatCompleted: null,
          isShowCompleteItem: null
        }).then();
      });
      return;
    }
    this.authService.getData('accessToken').then(accessToken => {
      this.accessToken = accessToken;
      this.recipeIngredientService.getAll(this.accessToken).subscribe((res: any) => {
        this.totalIngredients = 0;
        this.ingredientService.manageIngredientsData({
          realData: res.data,
          isCatCompleted: null,
          isShowCompleteItem: null
        }).then();
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

}
