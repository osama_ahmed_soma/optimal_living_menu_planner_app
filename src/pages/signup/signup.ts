import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, ToastController, Events} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook';
import {GooglePlus} from '@ionic-native/google-plus';

// Providers
import {UserProvider} from '../../providers/user/user';
import {AuthProvider} from '../../providers/auth/auth';
import {WelcomePage} from "../welcome/welcome";

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

  signUpList: {
    email: boolean,
    facebook: boolean,
    googlePlus: boolean
  } = {
    email: true,
    facebook: false,
    googlePlus: false
  };

  user: {
    name: string,
    email: string,
    password: string,
    password_confirmation: string
  } = {
    name: '',
    email: '',
    password: '',
    password_confirmation: ''
  };

  errors: {
    user: {
      name: {
        isError: boolean,
        error: string
      },
      email: {
        isError: boolean,
        error: string
      },
      password: {
        isError: boolean,
        error: string
      }
    }
  };

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private storage: Storage,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private userService: UserProvider,
              private events: Events,
              private fb: Facebook,
              private googlePlus: GooglePlus,
              private authService: AuthProvider) {
    this.setErrorsToDefault();
  }

  private setErrorsToDefault() {
    this.errors = {
      user: {
        name: {
          isError: false,
          error: ''
        },
        email: {
          isError: false,
          error: ''
        },
        password: {
          isError: false,
          error: ''
        }
      }
    };
  }

  private presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });

    toast.present();
  }

  back() {
    this.navCtrl.pop();
  }

  private manageSelectedData({collections, flagProperty, getProperty}) {
    if (collections.length > 0) {
      let tempCollections = [];
      for (let collection of collections) {
        if (collection.hasOwnProperty(flagProperty) && collection[flagProperty]) {
          tempCollections.push(collection[getProperty]);
        }
      }
      return tempCollections;
    }
    return collections;
  }

  private getAllStepsData(): any {
    return new Promise(async resolve => {
      resolve({
        diets: await this.storage.get('selectedDiet'),
        allergies: this.manageSelectedData({
          collections: await this.storage.get('selectedAllergies'),
          flagProperty: 'isSelected',
          getProperty: 'id'
        }),
        dislikes: this.manageSelectedData({
          collections: await this.storage.get('selectedDislikes'),
          flagProperty: 'isSelected',
          getProperty: 'id'
        }),
        servings: this.manageSelectedData({
          collections: await this.storage.get('activeServings'),
          flagProperty: 'isActive',
          getProperty: 'id'
        })[0]
      });
    });
  }

  private isChild(string, checkString) {
    return string.indexOf(checkString) >= 0;
  }

  private removeSignUpSessions() {
    this.storage.remove('selectedDiet');
    this.storage.remove('selectedAllergies');
    this.storage.remove('selectedDislikes');
    this.storage.remove('activeServings');
  }

  async singUp() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.setErrorsToDefault();
    const allStepsData = await this.getAllStepsData();
    // if (!this.user.email || !this.user.password) {
    //   loading.dismiss();
    //   return this.presentToast('Both fields are required.');
    // }
    // if (this.user.password !== this.user.password_confirmation) {
    //   loading.dismiss();
    //   return this.presentToast('Confirm Password is not same.');
    // }
    // this.user.servings = allStepsData.servings;
    // delete allStepsData.servings;
    const data = {
      user: this.user,
      ...allStepsData
    };
    this.userService.register(data).subscribe(() => {
      this.events.publish('user:login', this.user, loading, this.presentToast.bind(this), (res, loading) => {
        // remove signup sessions
        this.removeSignUpSessions();
        this.events.publish('user:get', res.access_token, {
          success: () => loading.dismiss(),
          err: () => loading.dismiss()
        });
        // this.navCtrl.setRoot('WelcomePage', {}, {
        //   animate: true,
        //   direction: 'forward'
        // });
        // loading.dismiss();
      });
    }, err => {
      loading.dismiss();
      const errors = err.error.errors;
      for (let error in errors) {
        if (this.isChild(error, '.')) {
          // split string
          const childProps = error.split(".");
          this.errors.user[childProps[1]] = {
            isError: true,
            error: errors[error][0]
          };
        } else {
          this.errors.user[error] = {
            isError: true,
            error: errors[error][0]
          };
        }
      }
      this.presentToast(err.error.message);
    });
  }

  private async loginWithProviderOnSuccess(res, loading) {
    const allStepsData = await this.getAllStepsData();
    this.userService.update(allStepsData, res.access_token).subscribe(() => {
      this.authService.setTokens(res);
      this.events.publish('user:get', res.access_token, {
        success: () => loading.dismiss(),
        err: () => loading.dismiss()
      });
      // this.navCtrl.setRoot('WelcomePage', {}, {
      //   animate: true,
      //   direction: 'forward'
      // });
      // loading.dismiss();
    }, err => {
      this.loginWithProviderOnError(err, loading);
    });
  }

  private loginWithProviderOnError(err, loading) {
    loading.dismiss();
    this.presentToast(err);
  }

  loginFacebook() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.events.publish('user:login:facebook', {
      loading,
      onSuccess: this.loginWithProviderOnSuccess.bind(this),
      onError: this.loginWithProviderOnError.bind(this)
    });
  }

  loginGooglePlus() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.events.publish('user:login:google', {
      loading,
      onSuccess: this.loginWithProviderOnSuccess.bind(this),
      onError: this.loginWithProviderOnError.bind(this)
    });
  }

}
