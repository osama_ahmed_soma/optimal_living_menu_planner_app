import {Component} from '@angular/core';
import {IonicPage, ViewController} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {ProPlanProvider} from '../../providers/pro-plan/pro-plan';

/**
 * Generated class for the UpgradePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-upgrade',
  templateUrl: 'upgrade.html',
})
export class UpgradePage {

  proPlans: Array<any> = [];

  constructor(private viewCtrl: ViewController, private authService: AuthProvider, private proPlanService: ProPlanProvider) {
    // get plans
    this.authService.getData('accessToken').then(accessToken => {
      this.proPlanService.getAll(accessToken).subscribe((plans: any) => {
        this.proPlans = plans.data;
      }, err => {
        console.log(err);
      });
    });
  }

  planSelected(index) {

  }

  close() {
    this.viewCtrl.dismiss();
  }

}
