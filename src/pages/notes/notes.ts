import {Component, ViewChild} from '@angular/core';
import {IonicPage, ViewController, NavParams, LoadingController} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {UserRecipeProvider} from '../../providers/user-recipe/user-recipe';

/**
 * Generated class for the NotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notes',
  templateUrl: 'notes.html',
})
export class NotesPage {

  @ViewChild('areaToFocus') areaToFocus;

  accessToken: string = '';

  recipeID: number = null;
  notes: string = null;
  actualNotes: string = null;

  constructor(private viewCtrl: ViewController,
              private navParams: NavParams,
              private userRecipeService: UserRecipeProvider,
              private authService: AuthProvider,
              private loadingCtrl: LoadingController) {
    this.actualNotes = this.navParams.get('notes');
    this.recipeID = this.navParams.get('recipeID');
    this.notes = this.actualNotes;
    this.authService.getData('accessToken').then(accessToken => (this.accessToken = accessToken));
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.areaToFocus.setFocus();
    }, 500);
  }

  add() {
    // now you can add notes
    let loading = this.loadingCtrl.create({});
    loading.present();
    this.userRecipeService.addNotes(this.accessToken, {recipeID: this.recipeID, notes: this.notes}).subscribe(() => {
      this.close(true);
      loading.dismiss();
    }, err => console.log(err));
  }

  close(isRefresh: boolean = false) {
    let notes = this.actualNotes.trim();
    if (isRefresh) {
      notes = this.notes.trim();
    }
    this.viewCtrl.dismiss({
      notes
    });
  }

}
