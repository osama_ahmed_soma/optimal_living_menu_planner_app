import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServingsPage } from './servings';

@NgModule({
  declarations: [
    ServingsPage,
  ],
  imports: [
    IonicPageModule.forChild(ServingsPage),
  ],
})
export class ServingsPageModule {}
