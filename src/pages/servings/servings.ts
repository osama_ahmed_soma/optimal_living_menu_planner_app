import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Storage} from '@ionic/storage';

// Providers
import {UserProvider} from '../../providers/user/user';
import {AuthProvider} from '../../providers/auth/auth';
import {ServingProvider} from '../../providers/serving/serving';

/**
 * Generated class for the ServingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-servings',
  templateUrl: 'servings.html',
})
export class ServingsPage {

  servings: Array<any>;

  afterLogin: boolean = false;
  totalSteps: number = 5;

  isLoading: boolean = true;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private storage: Storage,
              private loadingCtrl: LoadingController,
              private userService: UserProvider,
              private authService: AuthProvider,
              private servingService: ServingProvider) {
    this.afterLogin = this.navParams.get('afterLogin') || false;
    if (this.afterLogin) {
      this.totalSteps = 4;
    }
    this.servingService.getAll().subscribe((servings: any) => {
      this.servings = servings.data;
      setTimeout(() => {
        if (this.servings.length > 0) {
          this.servings[0].isActive = true;
        }
      }, 10);
      this.storage.get('activeServings').then((val) => {
        if (Array.isArray(val) && val.length > 0) {
          for (let serving of val) {
            if (serving.isActive) {
              setTimeout(() => {
                this.servingActive(serving.id);
              }, 10);
            }
          }
        }
        this.isLoading = false;
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServingsPage');
  }

  back() {
    // this.setServings();
    this.navCtrl.pop();
  }

  next() {
    this.setServings();
    if (this.afterLogin) {
      this.addAllData();
    } else {
      this.navCtrl.push('SignupPage');
    }
  }

  private manageSelectedData({collections, flagProperty, getProperty}) {
    if (collections.length > 0) {
      let tempCollections = [];
      for (let collection of collections) {
        if (collection.hasOwnProperty(flagProperty) && collection[flagProperty]) {
          tempCollections.push(collection[getProperty]);
        }
      }
      return tempCollections;
    }
    return collections;
  }

  private getAllStepsData(): any {
    return new Promise(async resolve => {
      resolve({
        diets: await this.storage.get('selectedDiet'),
        allergies: this.manageSelectedData({
          collections: await this.storage.get('selectedAllergies'),
          flagProperty: 'isSelected',
          getProperty: 'id'
        }),
        dislikes: this.manageSelectedData({
          collections: await this.storage.get('selectedDislikes'),
          flagProperty: 'isSelected',
          getProperty: 'id'
        }),
        servings: this.manageSelectedData({
          collections: await this.storage.get('activeServings'),
          flagProperty: 'isActive',
          getProperty: 'id'
        })[0]
      });
    });
  }

  private removeStepsSessions() {
    this.storage.remove('selectedDiet');
    this.storage.remove('selectedAllergies');
    this.storage.remove('selectedDislikes');
    this.storage.remove('activeServings');
  }

  private async addAllData() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    const allStepsData = await this.getAllStepsData();
    const accessToken = await this.authService.getData('accessToken');
    this.userService.update(allStepsData, accessToken).subscribe(() => {
      this.removeStepsSessions();
      this.navCtrl.setRoot('WelcomePage', {}, {
        animate: true,
        direction: 'forward'
      });
      loading.dismiss();
    });
  }

  private setServings() {
    this.storage.set('activeServings', this.servings);
  }

  servingActive(servingID) {
    if (this.servings.length > 0) {
      for (let serving of this.servings) {
        serving.isActive = serving.id === servingID;
      }
    }
  }

}
