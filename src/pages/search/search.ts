import { UtilityProvider } from './../../providers/utility/utility';
import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController, Events} from 'ionic-angular';


// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {DishTypeProvider} from '../../providers/dish-type/dish-type';
import {CourseProvider} from '../../providers/course/course';

// Helpers
import {HelperMethods} from '../../classes/HelperMethods';

// just for demo
import {RecipeProvider} from '../../providers/recipe/recipe';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {

  collapseData: {
    calories: {
      data: {
        cals: Array<any>,
        selected: number
      },
      is: boolean,
      isLoading: boolean
    },
    dishTypes: {
      data: Array<any>,
      is: boolean,
      isLoading: boolean
    },
    courses: {
      data: Array<any>,
      is: boolean,
      isLoading: boolean
    },
    sort: {
      data: Array<any>,
      is: boolean,
      isLoading: boolean
    },
    breakfast: {
      data: Array<any>,
      is: boolean,
      isLoading: boolean
    },
    lunch: {
      data: Array<any>,
      is: boolean,
      isLoading: boolean
    },
    dinner: {
      data: Array<any>,
      is: boolean,
      isLoading: boolean
    }
  } = {
    calories: {
      data: {
        cals: [
          {
            id: 1,
            value: 500,
            isChecked: false
          },
          {
            id: 2,
            value: 550,
            isChecked: false
          },
          {
            id: 3,
            value: 600,
            isChecked: false
          },
          {
            id: 4,
            value: 650,
            isChecked: false
          },
          {
            id: 5,
            value: 700,
            isChecked: false
          }
        ],
        selected: 0
      },
      is: true,
      isLoading: false
    },
    dishTypes: {
      data: [],
      is: false,
      isLoading: true
    },
    courses: {
      data: [],
      is: false,
      isLoading: true
    },
    sort: {
      data: [
        {
          id: 1,
          type: 'Name',
          values: [
            {
              title: 'ASC',
              value: 'asc',
              isSelected: false
            },
            {
              title: 'DESC',
              value: 'desc',
              isChecked: false
            }
          ],
          selected: ''
        },
        {
          id: 2,
          type: 'Latest',
          values: [
            {
              title: 'ASC',
              value: 'asc',
              isSelected: false
            },
            {
              title: 'DESC',
              value: 'desc',
              isSelected: false
            }
          ],
          selected: ''
        }
      ],
      is: false,
      isLoading: false
    },
    breakfast: {
      data: [],
      is: true,
      isLoading: false
    },
    lunch: {
      data: [],
      is: false,
      isLoading: true
    },
    dinner: {
      data: [],
      is: false,
      isLoading: false
    }
  };

  selectedDishTypesIDs: Array<number> = [];
  selectedCoursesIDs: Array<number> = [];
  selectedSorts: Array<{
    type: string,
    value: string
  }> = [];

  accessToken: string = '';

  isMeal = false;
  mealitem = {};
  isLoading: boolean = true;
  recipes: Array<any> = [];


  private startLoading(): void {
    this.isLoading = true;
  }

  private stopLoading(): void {
    this.isLoading = false;
  }



  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              public events: Events,
              private viewCtrl: ViewController,
              private dishTypeService: DishTypeProvider,
              private courseService: CourseProvider,
              private authService: AuthProvider,
              public util: UtilityProvider,

              // just for demo
              private recipeService: RecipeProvider,
              ) {

    this.isMeal = navParams.get('isMeal');
    var searchData = navParams.get('searchedData');
    console.log(this.isMeal);

    this.authService.getData('accessToken').then(accessToken => {
      this.accessToken = accessToken;

      if(this.isMeal){
        this.mealitem = navParams.get('mealitem');

        this.recipeService.getAllRecipeChild(this.mealitem['id'], this.accessToken).subscribe((recipes: any) => {
            console.log(recipes);
            //this.recipes = recipes;

              this.collapseData.breakfast.data = recipes["Breakfast"];
              this.collapseData.lunch.data = recipes["Lunch"];
              this.collapseData.dinner.data = recipes["Dinner"];

            if(!recipes["Breakfast"] &&
              !recipes["Lunch"] &&
              !recipes["Dinner"] ){

                util.showAlert("Recipes will be added soon in this program");
                this.viewCtrl.dismiss({});
                return;

            }







            this.stopLoading();
          }, err => {
            this.stopLoading();
          });

      }else{

        if(searchData != null){
          this.collapseData.calories.data.selected = searchData.calories;


          this.collapseData.sort.data.forEach(sor => {
            sor['values'].forEach(ins => {

              searchData.sort.forEach(ele => {
                console.log(ele['type'] , sor['type'] , ele['value'] , ins['value']);

                if(ele['type'] == sor['type'] && ele['value'] == ins['value']){
                  sor.selected = ins['value'];
                }
              });

            });
          });

        }

        // this.selectedSorts = searchData.sort;

        this.dishTypeService.getAll(this.accessToken).subscribe((res: any) => {
          this.collapseData.dishTypes.data = res.data;
          this.collapseData.dishTypes.isLoading = false;

          if(searchData != null){

            this.selectedDishTypesIDs = searchData.dishTypes;
            this.collapseData.dishTypes.data.forEach(dishType => {

              if(this.selectedDishTypesIDs.indexOf(dishType.id) !== -1){
                dishType.isChecked = true
              }else{
                dishType.isChecked = false
              }

            });

          }

        }, () => {}, () =>{

          this.courseService.getAll(this.accessToken).subscribe((res: any) => {
            this.collapseData.courses.data = res.data;
            this.collapseData.courses.isLoading = false;
            if(searchData != null){

              this.selectedCoursesIDs = searchData.courses;
              this.collapseData.courses.data.forEach(course => {

                if(this.selectedCoursesIDs.indexOf(course.id) !== -1){
                  course.isChecked = true
                }else{
                  course.isChecked = false
                }

              });

            }
          }, () => {}, () => {



          } );

        });





      }



    });
  }

  clickedOnCheck(recipe) {
    console.log(recipe);
    recipe.isChecked = !recipe.isChecked
    if(recipe.isChecked){
      this.addToMeanPlan(recipe)
    }else{
      this.removeFromMeanPlan(recipe)
    }

  }

  openRecipe(recipe) {
    // this.navCtrl.pop({}, () => {
      this.events.publish('Recipe:OpenFromQs', recipe);
    // });
  }

  addToMeanPlan(recipe) {
    this.events.publish('Recipe:Add', recipe);
  }

  removeFromMeanPlan(recipe) {
    this.events.publish('Recipe:Remove', recipe);
  }

  close(isDataSend: boolean = false) {
    this.viewCtrl.dismiss({
      calories: this.collapseData.calories.data.selected,
      dishTypes: this.selectedDishTypesIDs,
      courses: this.selectedCoursesIDs,
      sort: this.selectedSorts,
      isUpdated: isDataSend
    });
  }

  closeMealTypes(isDataSend: boolean = false) {
    this.viewCtrl.dismiss({
      plan: this.mealitem,
      isUpdated: isDataSend
    });
  }

  collapseToggle(type) {
    this.collapseData[type].is = !this.collapseData[type].is;
  }

  updateDishType(dishType) {
    if (dishType.isChecked) {
      if (this.selectedDishTypesIDs.indexOf(dishType.id) === -1) {
        this.selectedDishTypesIDs.push(dishType.id);
      }
    } else {
      if (this.selectedDishTypesIDs.indexOf(dishType.id) !== -1) {
        this.selectedDishTypesIDs.splice(this.selectedDishTypesIDs.indexOf(dishType.id), 1);
      }
    }
  }

  updateCourses(course) {
    if (course.isChecked) {
      if (this.selectedCoursesIDs.indexOf(course.id) === -1) {
        this.selectedCoursesIDs.push(course.id);
      }
    } else {
      if (this.selectedCoursesIDs.indexOf(course.id) !== -1) {
        this.selectedCoursesIDs.splice(this.selectedCoursesIDs.indexOf(course.id), 1);
      }
    }
  }

  updatedSort(sort) {
    if (sort.selected != '') {
      if (HelperMethods.isExistsInArrayByProperty({
          array: this.selectedSorts,
          property: 'type',
          value: sort.type
        })) {
        // this.selectedSorts[HelperMethods.getIndex(sort.type, this.selectedSorts)].value = sort.selected;
        this.selectedSorts[HelperMethods.getIndexOfArray({
          nameOfField: 'type',
          value: sort.type,
          array: this.selectedSorts
        })].value = sort.selected;
      } else {
        this.selectedSorts.push({
          type: sort.type,
          value: sort.selected
        });
      }
    }
  }

  apply() {
    this.close(true);
  }
  applymealtypes() {
    this.closeMealTypes(true);
  }

  // /**
  //  * Perform a service for the proper items.
  //  */
  // getItems(ev) {
  //   let val = ev.target.value;
  //   if (!val || !val.trim()) {
  //     this.currentItems = [];
  //     return;
  //   }
  //   this.currentItems = this.items.query({
  //     name: val
  //   });
  // }
  //
  // /**
  //  * Navigate to the detail page for this item.
  //  */
  // openItem(item: Item) {
  //   this.navCtrl.push('ItemDetailPage', {
  //     item: item
  //   });
  // }

}
