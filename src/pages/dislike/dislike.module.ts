import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DislikePage } from './dislike';

@NgModule({
  declarations: [
    DislikePage,
  ],
  imports: [
    IonicPageModule.forChild(DislikePage),
  ],
})
export class DislikePageModule {}
