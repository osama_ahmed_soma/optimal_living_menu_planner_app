import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ModalController} from 'ionic-angular';
import {Storage} from '@ionic/storage';

// Provider
import {IngredientProvider} from '../../providers/ingredient/ingredient';
import {HelperMethods} from "../../classes/HelperMethods";
import { DislikeSearchPage } from '../dislike-search/dislike-search';

/**
 * Generated class for the DislikePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dislike',
  templateUrl: 'dislike.html',
})
export class DislikePage {

  dislikes: Array<any> = [];
  moreDislikes: Array<any> = [];
  filterMoreDislikes: Array<any> = [];

  isLoading: boolean = true;

  afterLogin: boolean = false;
  totalSteps: number = 5;

  isShowMore: boolean = true;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private storage: Storage,
              private ingredientService: IngredientProvider,
              public modalCtrl: ModalController,
              private alertCtrl: AlertController) {
    this.afterLogin = this.navParams.get('afterLogin') || false;
    if (this.afterLogin) {
      this.totalSteps = 4;
    }
    this.ingredientService.getDislike().subscribe((res: any) => {



      let index = 0;
      for (let dislike of res.data) {
        // console.log(dislike)
        dislike.name = dislike.name.toLowerCase();
        if (index < 6) {
          this.dislikes.push(dislike);
        } else {
          this.moreDislikes.push(dislike);
        }
        index++;
      }

      this.moreDislikes.sort((a, b) => {
        if (a.name < b.name) return -1;
        else if (a.name > b.name) return 1;
        else return 0;
      });

      // this.dislikes = res.data;
      this.manageStoredData();
    });
  }

  private manageStoredData() {
    this.storage.get('selectedDislikes').then((val) => {
      if (Array.isArray(val) && val.length > 0) {
        for (let dislike of val) {
          if (dislike.isSelected) {
            this.activeDislike(dislike);
          }
        }
      }
      this.isLoading = false;
    });
  }

  openAll() {
    let alert = this.alertCtrl.create({
      enableBackdropDismiss: false
    });
    alert.setTitle('Remove This Food Item');
    for (let dislike of this.moreDislikes) {
      alert.addInput({
        type: 'checkbox',
        label: dislike.name,
        value: dislike,
        checked: false
      });
    }
    alert.addButton({
      text: 'Add',
      handler: selectedDislike => {
        if (selectedDislike.length <= 0) return;
        for (const dislike of selectedDislike) {
          const index = HelperMethods.getIndexOfArray({
            nameOfField: 'id',
            value: dislike.id,
            array: this.moreDislikes
          });
          dislike.isSelected = true;
          this.dislikes.push(dislike);
          this.moreDislikes.splice(index, 1);
        }
      }
    });
    alert.present();
  }

  private activeMoreDislikes(d) {
    let i = 0;
    for (let dislike of this.moreDislikes) {
      if (dislike.id === d.id) {
        dislike.isSelected = true;
        this.dislikes.push(dislike);
        this.moreDislikes.splice(i, 1);
        break;
      }
      i++;
    }
    if (this.moreDislikes.length <= 0) {
      this.isShowMore = false;
    }
  }

  private activeDislike(d) {
    for (let dislike of this.dislikes) {
      if (dislike.id === d.id) {
        dislike.isSelected = true;
        break;
      } else {
        this.activeMoreDislikes(d);
      }
    }
  }

  back() {
    this.navCtrl.pop();
  }

  next() {
    this.storage.set('selectedDislikes', this.dislikes);
    this.navCtrl.push('ServingsPage', {
      afterLogin: this.afterLogin
    });
  }

  openIngredientSearch(){
    let data = {dislikes: this.moreDislikes, heading: "Allergies"};
    let myModal = this.modalCtrl.create(DislikeSearchPage, data);
    myModal.onDidDismiss(data => {
      console.log(data);
      if(data){
        if(data.length > 0){
          for (const dislike of data) {
            const index = HelperMethods.getIndexOfArray({
              nameOfField: 'id',
              value: dislike.id,
              array: this.moreDislikes
            });
            dislike.isSelected = true;
            this.dislikes.push(dislike);
            this.moreDislikes.splice(index, 1);
          }
  
        }
      }
      
    });
    myModal.present();
  }


}
