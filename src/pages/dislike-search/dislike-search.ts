import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the DislikeSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dislike-search',
  templateUrl: 'dislike-search.html',
})
export class DislikeSearchPage {

  searchTerm: string = '';
  dislikes: any = [];
  items: any;
  heading: string = "";

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams) {

    var c = this.navParams.get('dislikes');
    this.heading = this.navParams.get('heading');
    
    this.dislikes = c;
    this.items = c;
    this.items.forEach((item) => {
      item['checked'] = false;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DislikeSearchPage');
  }

  setFilteredItems(){
    var self = this;
    this.items = this.dislikes.filter((item) => {
        return item['name'].toLowerCase().indexOf(self.searchTerm.toLowerCase()) > -1;
    });

  }

  returnSelected(){
    var c = this.items.filter((item) => {
      return (item['selected'] == true);
    });
    console.log(c);
    this.viewCtrl.dismiss(c);
  }

}
