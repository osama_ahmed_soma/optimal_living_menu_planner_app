import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DislikeSearchPage } from './dislike-search';

@NgModule({
  declarations: [
    DislikeSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(DislikeSearchPage),
  ],
})
export class DislikeSearchPageModule {}
