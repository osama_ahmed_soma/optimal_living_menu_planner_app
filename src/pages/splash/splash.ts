import {Component} from '@angular/core';
import {IonicPage, NavController, Platform, MenuController, Events} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';

// Providers
import {AuthProvider} from "../../providers/auth/auth";
import {UserProvider} from "../../providers/user/user";
import {PreferredDietPage} from "../preferred-diet/preferred-diet";

/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class SplashPage {

  constructor(public navCtrl: NavController,
              private platform: Platform,
              private splashScreen: SplashScreen,
              private menu: MenuController,
              private authService: AuthProvider,
              private userService: UserProvider,
              private events: Events) {
  }

  ionViewDidEnter() {
    this.platform.ready().then(() => {
      this.splashScreen.hide();
    });
    this.menu.swipeEnable(false);
    this.init();
  }

  private async init() {
    let page: string = 'TutorialPage';
    // get all data from local storage
    const storedData = await this.authService.getData();
    if (storedData.accessToken && storedData.refreshToken && storedData.expiresIn && storedData.added) {
      this.authService.setTokens(storedData, false);
    }
    if (storedData.accessToken) {
      // get user data
      this.authService.refresh().then(async res => {
        this.events.publish('user:get', storedData.accessToken, {
          success: null,
          err: () => this.redirect(page)
        });
      })
      .catch(err => { this.redirect(page); });

      ;
    } else {
      this.redirect(page);
    }
  }

  private redirect(page, opts = {}) {
    this.navCtrl.setRoot(page, opts);
  }

  ionViewDidLeave() {
    this.menu.swipeEnable(true);
  }

}
