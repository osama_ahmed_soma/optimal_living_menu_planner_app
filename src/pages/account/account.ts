import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {UserProvider} from '../../providers/user/user';

/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  accessToken: string = '';

  isLoading: boolean = true;

  userData: any = null;

  accountData: {
    name: string,
    password: string,
    confirmPassword: string
  } = {
    name: '',
    password: '',
    confirmPassword: ''
  };

  isUpdated: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private authService: AuthProvider,
              private userService: UserProvider,
              private toastCtrl: ToastController,) {
    this.authService.getData('accessToken').then(accessToken => {
      this.accessToken = accessToken;
      this.init();
    });
  }

  private handleError(err) {
    console.log(err);
  }

  private init() {
    this.userService.getUser(this.accessToken).subscribe((userData: any) => {
      this.userData = userData.data;
      this.accountData.name = this.userData.name;
      this.isLoading = false;
    }, this.handleError);
  }

  private presentToast(msg, className: string = 'error') {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      cssClass: className
    });

    toast.present();
  }

  private checkIsUpdated() {
    if (this.userData.name.trim() !== this.accountData.name.trim()) {
      this.isUpdated = true;
    } else {
      this.isUpdated = false;
    }
  }

  private validate() {
    return new Promise(resolve => {
      let isError = false;
      if (this.accountData.name.trim() === '') {
        this.presentToast('Name field is required.');
        isError = true;
      }
      if (this.accountData.password.trim() !== '' && (this.accountData.password.trim() !== this.accountData.confirmPassword.trim())) {
        this.presentToast('Passwords are not same.');
        isError = true;
      }
      if (!isError) {
        this.checkIsUpdated();
        resolve();
      }
    });
  }

  update() {
    this.validate().then(() => {
      if (this.isUpdated) {
        this.userService.update({
          name: this.accountData.name,
          password: this.accountData.password
        }, this.accessToken).subscribe();
      }
      this.navCtrl.pop();
    }).then(() => {
      console.log(123);
    });
  }

}
