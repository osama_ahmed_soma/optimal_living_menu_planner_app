import {Component} from '@angular/core';
import {IonicPage, NavController, ToastController, Events, LoadingController, MenuController} from 'ionic-angular';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook';
import {GooglePlus} from "@ionic-native/google-plus";

// Providers
import {UserProvider} from '../../providers/user/user';
import {AuthProvider} from '../../providers/auth/auth';
import {TutorialPage} from "../tutorial/tutorial";
import {WelcomePage} from "../welcome/welcome";
import {PreferredDietPage} from "../preferred-diet/preferred-diet";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  account: { email: string, password: string } = {
    email: '',
    password: ''
  };

  errors: {
    email: {
      isError: boolean,
      error: string
    },
    password: {
      isError: boolean,
      error: string
    }
  };

  constructor(private navCtrl: NavController,
              private menu: MenuController,
              private toastCtrl: ToastController,
              private events: Events,
              private userService: UserProvider,
              private authService: AuthProvider,
              private loadingCtrl: LoadingController,
              private fb: Facebook,
              private googlePlus: GooglePlus,) {
    this.initErrors();
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

  private presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });

    toast.present();
  }

  private initErrors() {
    this.errors = {
      email: {
        isError: false,
        error: ''
      },
      password: {
        isError: false,
        error: ''
      }
    };
  }

  private validate() {
    if (this.account.email == '') {
      this.errors.email = {
        isError: true,
        error: 'Email is required.'
      };
    } else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.account.email)) {
      this.errors.email = {
        isError: true,
        error: 'Email is not valid.'
      };
    }
    if (this.account.password == '') {
      this.errors.password = {
        isError: true,
        error: 'Password is required.'
      };
    }
  }

  protected loginSuccess(res, loading) {
    this.redirectAfterLogin(res, loading);
  }

  protected loginError() {
    // after error
  }

  private redirectAfterLogin(res, loading) {
    this.events.publish('user:get', res.access_token, {
      success: (user: any) => {
        // if (user.data.serving) {
        //   // redirect to welcome
        //   this.navCtrl.setRoot('WelcomePage', {}, {
        //     animate: true,
        //     direction: 'forward'
        //   });
        // } else {
        //   // redirect to steps
        //   this.navCtrl.setRoot('PreferredDietPage', {
        //     afterLogin: true
        //   }, {
        //     animate: true,
        //     direction: 'forward'
        //   });
        // }
        loading.dismiss();
      },
      err: err => {
        this.presentToast(err.message);
        loading.dismiss();
      }
    });
  }

  login() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.initErrors();
    this.validate();
    if (!this.errors.email.isError || !this.errors.password.isError) {
      this.events.publish('user:login', this.account, loading, this.presentToast.bind(this), this.loginSuccess.bind(this), this.loginError.bind(this));
    } else {
      loading.dismiss();
    }
  }

  private loginWithProviderOnSuccess(res, loading) {
    this.authService.setTokens(res);
    this.redirectAfterLogin(res, loading);
  }

  private loginWithProviderOnError(err, loading) {
    loading.dismiss();
    this.presentToast(err);
  }

  loginWithFacebook() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.events.publish('user:login:facebook', {
      loading,
      onSuccess: this.loginWithProviderOnSuccess.bind(this),
      onError: this.loginWithProviderOnError.bind(this)
    });
  }

  loginWithGooglePlus() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.events.publish('user:login:google', {
      loading,
      onSuccess: this.loginWithProviderOnSuccess.bind(this),
      onError: this.loginWithProviderOnError.bind(this)
    });
  }

  openSignUp() {
    this.navCtrl.setRoot('TutorialPage', {}, {
      animate: true,
      direction: 'back'
    });
  }

}
