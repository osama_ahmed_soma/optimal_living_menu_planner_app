import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the YvideoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-yvideo',
  templateUrl: 'yvideo.html',
})
export class YvideoPage {

  diet: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.diet = this.navParams.get('diet');
    console.log(this.diet);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YvideoPage');
  }

}
