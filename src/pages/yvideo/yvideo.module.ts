import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YvideoPage } from './yvideo';

@NgModule({
  declarations: [
    YvideoPage,
  ],
  imports: [
    IonicPageModule.forChild(YvideoPage),
  ],
})
export class YvideoPageModule {}
