// import * as Fraction from 'fraction.js';

// declare const Fraction: any;
declare const math: any;

export class HelperMethods {

  static isExistsInArrayByProperty({array, property, value}): boolean {
    return array.some(el => el[property] === value);
  }

  static getIndexOfArray({nameOfField, value, array}): number {
    for (let i = 0; i < array.length; i++) {
      if (array[i][nameOfField] === value) {
        return i;
      }
    }
  }

  static presentToast(message: string, styleClass: string = '', {toastCtrl}): void {
    if (message != '') {
      let toast = toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'top',
        cssClass: styleClass
      });
      toast.present();
    }
  }

  static isFloat(n) {
    return !isNaN(n) && n.toString().indexOf('.') != -1;
  }

  static improperFractionToMixedNumber(n, d) {
    const i = Math.floor(n / d);
      n -= i * d;
      return [i, n, d];
  }

  static getFraction(number: number) {
    if (HelperMethods.isFloat(number)) {
      const x = math.number(number);
      const a = math.fraction(x);
      var e = HelperMethods.improperFractionToMixedNumber(a.n, a.d)
      // const string: string = `${a.n}/${a.d}`;
      var str: string = e[0]+'<sup>'+ e[1] +'</sup>/<sub>'+e[2]+'</sub>​';
      if(e[0] == 0){
        if(e[2] == 100){
          str = (e[1]/e[2]).toFixed(2)
        }else{
          if(e[1] == 0){
            str = '';
          }else{
            str = '<sup>'+ e[1] +'</sup>/<sub>'+e[2]+'</sub>​';
          }

        }

      }else if(e[1] == 0){
        str = `${e[0]}`
      }

      // const string: string = `${e[0]} ${e[1]}/${e[2]}`;

      return str;
    }
    return number;
  }

  static getIngredientQuantityAndUnitHTML(ingredient) {
    let html = '';
    html += (ingredient.quantity) ? HelperMethods.getFraction(ingredient.quantity) : '';

    if(Math.floor(ingredient.sub_quantity) > 0){
      html += (ingredient.sub_quantity || ingredient.sub_unit) ? ' (' : '';
      html += (ingredient.sub_quantity) ? HelperMethods.getFraction(ingredient.sub_quantity)  : '';
      html += (ingredient.sub_unit) ? ' ' + ingredient.sub_unit.symbol : '';
      html += (ingredient.sub_quantity || ingredient.sub_unit) ? ')' : '';
    }


    html += (ingredient.unit) ? ' ' + ingredient.unit.symbol : '';
    return html;
  }

  static getIngredientQuantityAndUnitHTMLViaServing(_rs, _us, ingredient) {

    let rs = parseInt(_rs);
    let us = parseInt(_us);
    // console.log(ingredient)
    var quan = ingredient.quantity
    // decide if quantity remain same or change
    if( rs != us && quan != ''){

      // example recipe 2 - user 2
      if( rs == 2 && us == 2 ){
        quan = quan
      }

      // example recipe 2 - user 4
      if( rs == 2 && us == 4 ){
        quan = quan * 2
      }

      // example recipe 2 - user 6
      if( rs == 2 && us == 6 ){
        quan = quan * 3
      }

      // example recipe 4 - user 2
      if( rs == 4 && us == 2 ){
        quan = ( quan / 2 ).toFixed(2)
      }

      // example recipe 4 - user 4
      if( rs == 4 && us == 4 ){
        quan = quan
      }

      // example recipe 4 - user 6
      if( rs == 4 && us == 6 ){
        quan = ( quan / 1.5 ).toFixed(2)
      }

      // example recipe 6 - user 2
      if( rs == 6 && us == 2 ){
        quan = ( quan / 3 ).toFixed(2)
      }

      // example recipe 6 - user 4
      if( rs == 6 && us == 4 ){
        quan = ( quan * (2/3) ).toFixed(2)
      }

      // example recipe 6 - user 6
      if( rs == 6 && us == 6 ){
        quan = quan
      }


    }




    let html = '';
    html += (quan) ? HelperMethods.getFraction(quan) : '';

    if(Math.floor(ingredient.sub_quantity) > 0){
      html += (ingredient.sub_quantity || ingredient.sub_unit) ? ' (' : '';
      html += (ingredient.sub_quantity) ? HelperMethods.getFraction(ingredient.sub_quantity)  : '';
      html += (ingredient.sub_unit) ? ' ' + ingredient.sub_unit.symbol : '';
      html += (ingredient.sub_quantity || ingredient.sub_unit) ? ')' : '';
    }


    html += (ingredient.unit) ? ' ' + ingredient.unit.symbol : '';
    return html;
  }

  static imageExists(image_url: string = ''): boolean {
    const http = new XMLHttpRequest();
    http.open('HEAD', image_url, false);
    http.send();
    return http.status != 404;
  }

  static getDefaultImage(icon?: string, path?: string): string {
    return (icon && HelperMethods.imageExists(path)) ? path : `assets/img/default_recipe.jpg`;
  }

  static generateSubscriptionAlertOptions(s, c) {
    return {
      enableBackdropDismiss: false,
      title: 'This is a pro-only recipe',
      cssClass: 'pro_plan_recipe_alert_container',
      message: HelperMethods.getSubscriptionAlertHTML(),
      buttons: [
        {
          text: 'Not Now',
          role: 'cancel',
          handler: () => {
            if (c) {
              c();
            }
          }
        },
        {
          text: 'Learn More',
          handler: s
        }
      ]
    };
  }

  static generateShareAlertOptions(t, s) {
    return {
      enableBackdropDismiss: false,
      title: 'Share a link to this recipe',
      cssClass: 'pro_plan_recipe_alert_container',
      message: HelperMethods.getShareAlertHTML(t),
      buttons: [
        {
          text: 'OK',
          handler: s
        }
      ]
    };
  }

  static getShareAlertHTML(text: any) {
    var t = "<div>"
      +"<h6 >"+text+"</h6>"
      +"<ul>"
      +"<li>Your friends can cook this recipe too! Simply share this link via email, text message, Facebook, twitter etc</li>"
      +"<li>"+text+"</li>"
      +"<li>(We've copied this link to your clipboard so you can paste it easily)</li>"
      +"</ul>"
    +"</div>";

    return t;
  }

  static getSubscriptionAlertHTML() {
    return `<div >
  <h6 >UPGRADE TO PRO TODAY TO UNLOCK:</h6>
  <ul>
    <li>Exclusive recipes</li>
    <li>View nutritional information</li>
    <li>Calorie customization filters</li>
    <li>Add notes to recipe</li>
    <li>View your previous meal plan</li>
    <li>World class support</li>
  </ul>
</div>`;
  }

}
