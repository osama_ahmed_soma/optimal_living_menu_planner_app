export class ProviderAuth {

  private providerService;
  private userService;
  private providerName: string = 'facebook';
  private accessToken: string = '';

  private providerData;

  constructor(providerService, userService) {
    this.providerService = providerService;
    this.userService = userService;
  }

  setProvider(providerName = this.providerName) {
    this.providerName = providerName;
    return this;
  }

  login(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        resolve(await this[`${this.providerName}Login`]());
      } catch (e) {
        reject(e);
      }
    });
  }

  logout() {
    this[`${this.providerName}Logout`]();
  }

  private setProviderData(value) {
    this.providerData = value;
  }

  private providerUserLogin(methodName: string = 'loginWithFacebook') {
    return new Promise((resolve, reject) => {
      this.userService[methodName]({
        access_token: this.accessToken
      }).subscribe(res => {
        resolve({
          ...res,
          providerName: this.providerName,
          provider: this.providerData
        });
      }, err => {
        reject({
          message: err.error.message
        });
      });
    });
  }

  private facebookLogin() {
    return new Promise(async (resolve, reject) => {
      try {
        let res = await this.providerService.getLoginStatus();
        if (res.status === 'connected') {
          this.accessToken = res.authResponse.accessToken;
          this.setProviderData(res.authResponse);
          resolve(await this.providerUserLogin('loginWithFacebook'));
        } else {
          res = await this.providerService.login(['public_profile', 'user_friends', 'email']);
          if (res.status == 'connected') {
            this.accessToken = res.authResponse.accessToken;
            this.setProviderData(res.authResponse);
            resolve(await this.providerUserLogin('loginWithFacebook'));
          } else {
            reject('Not LoggedIn');
          }
        }
      } catch (e) {
        reject(e.message);
      }
    });
  }

  private googlePlusLogin() {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await this.providerService.trySilentLogin({});
        this.accessToken = res.accessToken;
        this.setProviderData(res);
        resolve(await this.providerUserLogin('loginWithGooglePlus'));
      } catch (e) {
        try {
          const res = await this.providerService.login({});
          this.accessToken = res.accessToken;
          this.setProviderData(res);
          resolve(await this.providerUserLogin('loginWithGooglePlus'));
        } catch (e) {
          reject(e.message);
        }
      }
    });
  }

  private facebookLogout() {
    this.providerService.logout();
  }

  private googlePlusLogout() {
    this.providerService.logout();
  }

}
