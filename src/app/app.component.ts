import {Component, ViewChild} from '@angular/core';
import {StatusBar} from '@ionic-native/status-bar';
import {Config, Nav, Platform, Events, ModalController} from 'ionic-angular';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook';
import {GooglePlus} from "@ionic-native/google-plus";
import {ScreenOrientation} from '@ionic-native/screen-orientation';
import {Storage} from '@ionic/storage';

// Providers
import {AuthProvider} from "../providers/auth/auth";
import {UserProvider} from '../providers/user/user';

// Classes
import {ProviderAuth} from "../classes/ProviderAuth";
import {ItemCreatePage} from "../pages/item-create/item-create";
import {SubscriptionSuccessPage} from "../pages/subscription-success/subscription-success";

@Component({
  template: `
    <ion-menu [content]="content">
      <ion-header>
        <ion-toolbar>
          <ion-title>Pages</ion-title>
        </ion-toolbar>
      </ion-header>

      <ion-content>
        <ion-list>
          <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
            {{p.title}}
          </button>
        </ion-list>
      </ion-content>

    </ion-menu>
    <ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage = 'SplashPage';

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    {title: 'Tutorial', component: 'TutorialPage'},
    {title: 'Welcome', component: 'WelcomePage'},
    {title: 'Tabs', component: 'TabsPage'},
    {title: 'Cards', component: 'CardsPage'},
    {title: 'Content', component: 'ContentPage'},
    {title: 'Login', component: 'LoginPage'},
    {title: 'Signup', component: 'SignupPage'},
    {title: 'Master Detail', component: 'ListMasterPage'},
    {title: 'Menu', component: 'MenuPage'},
    {title: 'Settings', component: 'SettingsPage'},
    {title: 'Search', component: 'SearchPage'},
    {title: 'Item Create', component: 'ItemCreatePage'},
    {title: 'Subscription Success', component: 'SubscriptionSuccessPage', modal: true},
    {title: 'Logout', method: 'logout'}
  ];

  constructor(private platform: Platform,
              private config: Config,
              private statusBar: StatusBar,
              private events: Events,
              private authService: AuthProvider,
              private fb: Facebook,
              private googlePlus: GooglePlus,
              private screenOrientation: ScreenOrientation,
              private userService: UserProvider,
              private modalCtrl: ModalController,
              private storage: Storage) {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      if (this.platform.is('cordova')) {
        this.screenOrientation.lock('portrait');
      }
      this.statusBar.styleDefault();
    });
    // assign events
    this.assignEvents();
  }

  private assignEvents() {
    this.events.subscribe('user:logout', this.logout.bind(this));
    this.events.subscribe('user:login', this.login.bind(this));
    this.events.subscribe('user:login:facebook', this.facebookLogin.bind(this));
    this.events.subscribe('user:login:google', this.googleLogin.bind(this));
    this.events.subscribe('user:get', this.getUser.bind(this));
  }

  private getUser(access_token, {success, err}) {
    this.userService.getUser(access_token).subscribe((user: any) => {
      this.storage.set('menu_planner_user_data', user.data);
      if (user.data.serving) {
        // redirect to welcome
        this.nav.setRoot('WelcomePage', {}, {
          animate: true,
          direction: 'forward'
        });
      } else {
        // redirect to steps
        this.nav.setRoot('PreferredDietPage', {
          afterLogin: true
        }, {
          animate: true,
          direction: 'forward'
        });
      }
      if (success) {
        success(user);
      }
    }, err)
  }

  private login(user, loading, presentToast, cb, onError = null) {
    const data = {
      email: user.email,
      password: user.password
    };
    this.userService.login(data).subscribe((res: any) => {
      this.userService.updateProviderDirect(res.access_token).subscribe();
      this.authService.setTokens({
        ...res,
        providerName: '',
        providerData: null
      });
      cb(res, loading);
    }, error => {
      loading.dismiss();
      if (error.error.hasOwnProperty('hint')) {
        presentToast(error.error.hint);
      } else {
        presentToast(error.error.message);
      }
      if (onError) {
        onError();
      }
    });
  }

  private facebookLogin({loading, onSuccess, onError}) {
    new ProviderAuth(this.fb, this.userService)
      .setProvider('facebook')
      .login()
      .then(res => {
        onSuccess(res, loading);
      }, err => {
        onError(err, loading);
      });
  }

  private googleLogin({loading, onSuccess, onError}) {
    new ProviderAuth(this.googlePlus, this.userService)
      .setProvider('googlePlus')
      .login()
      .then(res => {
        onSuccess(res, loading);
      }, err => {
        onError(err, loading);
      });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.hasOwnProperty('modal') && page.modal) {
      let modal = this.modalCtrl.create(page.component);
      modal.present();
      return;
    }
    if (page.hasOwnProperty('component')) {
      this.nav.setRoot(page.component);
      return;
    }
    if (page.hasOwnProperty('method')) {
      this[page.method]();
    }
  }

  logout() {
    this.authService.getData('providerName').then(providerName => {
      if (providerName != '') {
        let providerService: any = this.fb;
        if (providerName === 'googlePlus') {
          providerService = this.googlePlus;
        }
        const logoutFromProvider = new ProviderAuth(providerService, null);
        logoutFromProvider.setProvider(providerName).logout();
      }
      this.authService.removeData();
      this.storage.remove('menu_planner_user_data');
    });
    this.nav.setRoot('TutorialPage');
  }

}
