import {HttpClientModule} from '@angular/common/http';
import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {Camera} from '@ionic-native/camera';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {IonicStorageModule} from '@ionic/storage';
import {Facebook} from '@ionic-native/facebook';
import {GooglePlus} from '@ionic-native/google-plus';
import {ScreenOrientation} from '@ionic-native/screen-orientation';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import { Clipboard } from '@ionic-native/clipboard';

import {MyApp} from './app.component';
import {DietProvider} from '../providers/diet/diet';
import {AllergyProvider} from '../providers/allergy/allergy';
import {IngredientProvider} from '../providers/ingredient/ingredient';
import {UserProvider} from '../providers/user/user';
import {AuthProvider} from '../providers/auth/auth';
import {RecipeProvider} from '../providers/recipe/recipe';
import {DishTypeProvider} from '../providers/dish-type/dish-type';
import {UserRecipeProvider} from '../providers/user-recipe/user-recipe';
import {RecipeIngredientProvider} from '../providers/recipe-ingredient/recipe-ingredient';
import {IngredientTypeProvider} from '../providers/ingredient-type/ingredient-type';
import {UnitProvider} from '../providers/unit/unit';
import {UserIngredientProvider} from '../providers/user-ingredient/user-ingredient';
import {FavouriteProvider} from '../providers/favourite/favourite';
import {ServingProvider} from '../providers/serving/serving';
import { ProPlanProvider } from '../providers/pro-plan/pro-plan';
import { UtilityProvider } from '../providers/utility/utility';
import { YvideoPage } from "../pages/yvideo/yvideo"
import { DislikeSearchPage } from "../pages/dislike-search/dislike-search"

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CourseProvider } from '../providers/course/course';
@NgModule({
  declarations: [
    MyApp,
    YvideoPage,
    DislikeSearchPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    YvideoPage,
    DislikeSearchPage
  ],
  providers: [
    Camera,
    SplashScreen,
    StatusBar,
    Facebook,
    GooglePlus,
    ScreenOrientation,
    Clipboard,
    // Keep this to enable Ionic's runtime error handling during development
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DietProvider,
    AllergyProvider,
    IngredientProvider,
    UserProvider,
    AuthProvider,
    RecipeProvider,
    DishTypeProvider,
    UserRecipeProvider,
    RecipeIngredientProvider,
    IngredientTypeProvider,
    UnitProvider,
    UserIngredientProvider,
    FavouriteProvider,
    ServingProvider,
    ProPlanProvider,
    InAppBrowser,
    UtilityProvider,
    CourseProvider,

  ]
})
export class AppModule {
}
